package ltlFxGUI;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Rex Cope
 *
 * Wrapper for running JavaMop
 */
class JavamopWrapper {

    // Run JavaMop given a single mop file and destination for aj file
    static String runJMop(File inputMopFile, File outputMopDir, File workingDirectory) throws IOException {
        String messagesFromRunning = "";

        Process process;

        String javaMopStringToRun;
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            javaMopStringToRun = "javamop.bat";
        } else {
            javaMopStringToRun = "javamop";
        }


        // Get relative path to mop file and output mop directory
        String relativePathToMopFile = workingDirectory.toPath().relativize(inputMopFile.toPath()).toString();
        String relativePathToMopDir = workingDirectory.toPath().relativize(outputMopDir.toPath()).toString();

        ProcessBuilder builder;

        if (!relativePathToMopDir.equals("")) {
            messagesFromRunning += "> " + javaMopStringToRun + " " + "\"" + relativePathToMopFile + "\"" + " -d " + "\"" + relativePathToMopDir + "\"" + "\n";

            builder = new ProcessBuilder(
                    javaMopStringToRun, relativePathToMopFile, "-d", relativePathToMopDir);
        } else {
            messagesFromRunning += "> " + javaMopStringToRun + " " + "\"" + relativePathToMopFile + "\"" + "\n";

            builder = new ProcessBuilder(
                    javaMopStringToRun, relativePathToMopFile);
        }
        builder.directory(workingDirectory);
        builder.redirectErrorStream(true);
        process = builder.start();


        InputStream inputStream = process.getInputStream();
        InputStream errorInputStream = process.getErrorStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        InputStreamReader errorInputStreamReader = new InputStreamReader(errorInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        BufferedReader errorBufferedReader = new BufferedReader(errorInputStreamReader);
        String line;


        while ((line = bufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        while ((line = errorBufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        messagesFromRunning += "\n";


        System.out.println(messagesFromRunning);

        return messagesFromRunning;
    }

    // Extract BaseAspect from an aspect created by JavaMop and give it its own file
    static void extractBaseAspect(File ajFile) throws IOException {
        String baseAspectContents = "";
        String packageLine = "package";
        String startLine = "aspect BaseAspect {";
        String endLine = "}";
        String mainAspectContents = "";
        boolean packageLineFound = false;

        Scanner scanner = new Scanner(ajFile);

        // Search for needed lines line by line
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!packageLineFound && line.contains(packageLine)) {
                baseAspectContents += line + "\n\n";
                packageLineFound = true;
            } else if(line.contains(startLine)) {
                baseAspectContents += line + "\n";
                while (scanner.hasNextLine() && !line.contains(endLine)) {
                    line = scanner.nextLine();
                    baseAspectContents += line + "\n";
                }
                continue;
            }
            mainAspectContents += line + "\n";
        }
        scanner.close();

        createBaseAspect(baseAspectContents, ajFile.getParentFile());

        // Recreate original aspect file with BaseAspect removed
        FileWriter fw = new FileWriter(ajFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(mainAspectContents);
        bw.close();
    }

    // Create BaseAspect aspect file with given contents and directory
    private static void createBaseAspect(String contents, File directory) throws IOException {
        File baseAspectFile = new File(directory.getPath() + File.separator + "BaseAspect.aj");
        // if file doesn't exists, then create it
        if (!baseAspectFile.exists()) {
            boolean fileCreated = baseAspectFile.createNewFile();
            if (!fileCreated)
                throw new IOException("BaseAspect.aj file creation failed");
        }
        FileWriter fw = new FileWriter(baseAspectFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(contents);
        bw.close();
    }
}
