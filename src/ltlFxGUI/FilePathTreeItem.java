package ltlFxGUI;

import javafx.scene.control.TreeItem;
import ltlAgentPkg.LTLAgent;
import ltlAgentPkg.LTLFile;

/**
 * Created by Rex Cope
 */
class FilePathTreeItem extends TreeItem<String> {



    private boolean isDirectory;

    private boolean isFile;

    private boolean isLTL;

    boolean isDirectory() {
        return (this.isDirectory);
    }

    public boolean isFile() {
        return this.isFile;
    }

    public boolean isLTL() {
        return this.isLTL;
    }

    void setDirectory() {
        this.isDirectory = true;
        this.isFile = false;
        this.isLTL = false;
    }

    void setFile() {
        this.isFile = true;
        this.isDirectory = false;
        this.isLTL = false;
    }

    void setLTL() {
        this.isLTL = true;
        this.isFile = false;
        this.isDirectory = false;
    }

    // Event to run if item selected
    private Command event;

    public Command getEvent() {
        return this.event;
    }

    public void setEvent(Command event) {
        this.event = event;
    }

    FilePathTreeItem(String value) {
        super(value);
    }

    private String fullPath;    // Path to current file or folder

    void setFullPath(String path) {
        this.fullPath = path;
    }

    String getFullPath() {
        return this.fullPath;
    }

    private LTLFile ltlFile;       // File represented

    void setLTLFile(LTLFile file) {
        this.ltlFile = file;
    }

    LTLFile getLTLFile() {
        return this.ltlFile;
    }

    private LTLAgent ltl;   // LTL represented

    public void setLTL(LTLAgent ltl) {
        this.ltl = ltl;
    }

    public LTLAgent getLtl() {
        return this.ltl;
    }
}