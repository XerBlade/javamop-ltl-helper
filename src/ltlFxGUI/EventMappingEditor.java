package ltlFxGUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ltlAgentPkg.Event;
import ltlAgentPkg.Parameter;

import java.util.ArrayList;

/**
 * Created by Rex Cope
 */
class EventMappingEditor {
    private static Event thisEvent;     // This event
    private static String answer;  // Whether user clicked OK
    private static Button okButton = new Button("Save");
    private static Label headerLabel = new Label("");

    static String display(Event event, ArrayList<Parameter> staticParameters)
    {
        thisEvent = event.copy();

        Stage window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);
        window.setTitle("Event Mapping");

        Label nameLabel = new Label(thisEvent.getName());




        Label contentLabel = new Label("Definition");

        ScrollPane pointcutBox = new ScrollPane();
        pointcutBox.fitToHeightProperty().setValue(true);
        pointcutBox.setPrefViewportHeight(100);
        pointcutBox.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
        VBox pointcutBoxInner = new VBox();

        PointcutContainer pointcutContainer = new PointcutContainer(thisEvent, staticParameters, new AddParameterAction(), new RefreshAction(), okButton);

        // Label showing event header
        SetHeaderLabelText();

        pointcutBoxInner.getChildren().addAll(pointcutContainer, headerLabel);

        pointcutBox.setContent(pointcutBoxInner);

        Label packageLabel = new Label("Packages needed for advice");
        TextField eventPackages = new TextField();
        eventPackages.setText(PackageListHandler.getPackagesString(thisEvent.getAdvicePackages()));
        eventPackages.setPromptText("Enter packages as comma separated list");

        Label actionLabel = new Label("Advice");
        TextArea eventAction = new TextArea();
        eventAction.setText(thisEvent.getAction());





        okButton.setOnAction(e -> {

            event.getAdvicePackages().clear();
            event.getAdvicePackages().addAll(PackageListHandler.getPackagesList(eventPackages.getText()));
            event.setAction(eventAction.getText());

            event.pointcut = thisEvent.pointcut;
            event.setWhen(thisEvent.getWhen());
            event.setAfterParameter(thisEvent.getAfterParameter());
            answer = "Save";
            window.close();
        });




        window.setOnCloseRequest(we -> answer = "Cancel");

        VBox layout = new VBox(10);
        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(okButton);

        // Add everything
        layout.getChildren().addAll(nameLabel, contentLabel, pointcutBox, packageLabel, eventPackages, actionLabel, eventAction, buttons);
        Scene scene = new Scene(layout);
        scene.getStylesheets().add("main.css");
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }

    private static void SetHeaderLabelText() {
        thisEvent.generateUsedParameters();
        headerLabel.setText(thisEvent.getHeaderRepresentation());
    }

    private static class RefreshAction implements Command
    {
        @Override
        public void execute(Object data) {
            SetHeaderLabelText();
        }
    }

    private static class AddParameterAction implements Command
    {

        @SuppressWarnings("unchecked")
        @Override
        public void execute(Object data) {

            Parameter parameter = new Parameter();
            String result = ParameterEditor.display(parameter);

            if (result.equals("Save")) {
                thisEvent.parameters.add(parameter);
                Object[] args = new Object[]{((ArrayList<Object>) data).get(1), parameter};
                callCommand((Command) ((ArrayList<Object>) data).get(0), args);
            }
        }
    }

    // Execute command passed in with passed data as parameter
    private static void callCommand(Command command, Object[] data)
    {
        command.execute(data);
    }
}
