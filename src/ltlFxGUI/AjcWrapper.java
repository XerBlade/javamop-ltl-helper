package ltlFxGUI;



import java.io.*;

/**
 * Created by Rex Cope
 *
 * Wrapper for running AJC compiler within program
 */
class AjcWrapper {



    static String runAjc(File workingDirectory) throws IOException {
        String messagesFromRunning = "";

        Process process;


        String ajcStringToRun;
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            ajcStringToRun = "ajc.bat";
        } else {
            ajcStringToRun = "ajc";
        }



        messagesFromRunning += "> " + ajcStringToRun + " -1.7" + " -sourceroots " + "\"" + workingDirectory + "\"" + "\n";

        ProcessBuilder builder = new ProcessBuilder(ajcStringToRun, "-1.7", "-sourceroots", workingDirectory.toString());
        builder.directory(workingDirectory);
        builder.redirectErrorStream(true);
        process = builder.start();


        InputStream inputStream = process.getInputStream();
        InputStream errorInputStream = process.getErrorStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        InputStreamReader errorsInputStreamReader = new InputStreamReader(errorInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        BufferedReader errorBufferedReader = new BufferedReader(errorsInputStreamReader);
        String line;


        while ((line = bufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        while ((line = errorBufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        messagesFromRunning += "\n";

        System.out.println(messagesFromRunning);

        return messagesFromRunning;
    }

}
