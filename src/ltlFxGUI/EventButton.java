package ltlFxGUI;

import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import ltlAgentPkg.Event;

/**
 * Created by Xer
 */
class EventButton extends StackPane {
    private Button eventButton;     // Actual button to edit event
    Event event;            // Event represented

    EventButton(Event event)
    {
        this.event = event;

        eventButton = new Button(event.getName());

        this.getChildren().add(eventButton);
    }

    Button getEventButton()
    {
        return this.eventButton;
    }

    public Event getEvent()
    {
        return this.event;
    }
}
