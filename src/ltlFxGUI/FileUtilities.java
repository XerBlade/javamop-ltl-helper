package ltlFxGUI;

import java.io.File;
import java.io.IOException;

/**
 * Created by Rex Cope
 */
class FileUtilities {

    // Removes a directory and everything in it
    @SuppressWarnings("WeakerAccess")
    static void removeDirectory(File dir) throws IOException {
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null && files.length > 0) {
                for (File aFile : files) {
                    removeDirectory(aFile);
                }
            }
            boolean deleted = dir.delete();
            if (!deleted)
                throw new IOException("Unable to access folder " + dir.getName());
        } else {
            boolean deleted = dir.delete();
            if (!deleted)
                throw new IOException("Unable to access file " + dir.getName());
        }
    }

    // Cleans the files from a directory
    static void cleanDirectory(File dir) throws IOException {
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null && files.length > 0) {
                for (File aFile : files) {
                    String fileExtension = getFileExtension(aFile);
                    if (aFile.getName().contains("RuntimeMonitor.class") || aFile.getName().contains("MonitorAspect.class") || aFile.getName().contains("Monitor.class") || aFile.getName().contains("Monitor_Set.class") || aFile.getName().contains("DisableHolder.class") || aFile.getName().contains("BaseAspect") || aFile.getName().contains("MonitorAspect.aj") || fileExtension.equals("rvm") || aFile.getName().contains("RuntimeMonitor.java")) {
                        removeDirectory(aFile);
                    } else if (aFile.isDirectory()) {
                        cleanDirectory(aFile);
                    }
                }
            }
        }
    }

    // Get file extension of file
    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }

}
