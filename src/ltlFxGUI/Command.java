package ltlFxGUI;

/**
 * Created by Rex Cope
 * Used to simulate passing method between objects
 */
interface Command {
    void execute(Object data);
}
