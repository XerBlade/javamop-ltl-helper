package ltlFxGUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ltlAgentPkg.Parameter;

/**
 * Created by Rex Cope
 */
class ParameterEditor {
    private static String answer;  // Whether user clicked OK
    private static Boolean typeIsEmpty, nameIsEmpty;

    static String display(Parameter parameter)
    {

        Stage window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);
        window.setTitle("Parameter Editor");

        Label typeLabel = new Label("Type");
        TextField parameterType = new TextField();
        parameterType.setText(parameter.getType());
        typeIsEmpty = parameterType.getText().trim().isEmpty();

        Label nameLabel = new Label("Name");
        TextField parameterName = new TextField();
        parameterName.setText(parameter.getName());
        nameIsEmpty = parameterName.getText().trim().isEmpty();

        Label packageLabel = new Label("Package (if needed)");
        TextField parameterPackage = new TextField();
        parameterPackage.setText(parameter.getPackage());

        Label descriptionLabel = new Label("Description");
        TextArea parameterDescription = new TextArea();
        parameterDescription.setText(parameter.getDescription());

        Button okButton = new Button("Save");
        Button removeButton = new Button("Remove");

        okButton.setOnAction(e -> {
            parameter.setType(parameterType.getText());
            parameter.setName(parameterName.getText());
            parameter.setPackage(parameterPackage.getText());
            parameter.setDescription(parameterDescription.getText());
            answer = "Save";
            window.close();
        });

        removeButton.setOnAction(e -> {
            answer = "Remove";
            window.close();
        });

        window.setOnCloseRequest(we -> answer = "Cancel");

        VBox layout = new VBox(10);
        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(okButton, removeButton);

        // Enable/Disable save button depending on whether something is entered
        okButton.setDisable(typeIsEmpty || nameIsEmpty);

        // Do some validation
        parameterType.textProperty().addListener((observable, oldValue, newValue) -> {
            typeIsEmpty = newValue.trim().isEmpty();
            okButton.setDisable(typeIsEmpty || nameIsEmpty);
        });
        parameterName.textProperty().addListener((observable, oldValue, newValue) -> {
            nameIsEmpty = newValue.trim().isEmpty();
            okButton.setDisable(typeIsEmpty || nameIsEmpty);
        });

        // Add everything
        layout.getChildren().addAll(typeLabel, parameterType, nameLabel, parameterName, packageLabel, parameterPackage, descriptionLabel, parameterDescription, buttons);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }
}
