package ltlFxGUI;


import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ltlAgentPkg.Event;

/**
 * Created by Rex Cope
 */

class EventEditor {
    private static Event thisEvent;     // This event
    private static String answer;  // Whether user clicked OK
    private static Button okButton = new Button("Save");

    static String display(Event event)
    {
        thisEvent = event.copy();

        Stage window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);
        window.setTitle("Event Editor");

        Label nameLabel = new Label("Name");
        TextField eventName = new TextField();
        eventName.setText(thisEvent.getName());

        eventName.textProperty().addListener((observable, oldValue, newValue) -> thisEvent.setName(newValue));


        Label descriptionLabel = new Label("Description");
        TextArea eventDescription = new TextArea();
        eventDescription.setText(thisEvent.getDescription());

        Button removeButton = new Button("Remove");

        okButton.setOnAction(e -> {
            event.setName(eventName.getText());
            event.setDescription(eventDescription.getText());
            event.pointcut = thisEvent.pointcut;
            event.setWhen(thisEvent.getWhen());
            event.setAfterParameter(thisEvent.getAfterParameter());
            answer = "Save";
            window.close();
        });

        removeButton.setOnAction(e -> {
            answer = "Remove";
            window.close();
        });


        window.setOnCloseRequest(we -> answer = "Cancel");

        VBox layout = new VBox(10);
        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(okButton, removeButton);

        // Add everything
        layout.getChildren().addAll(nameLabel, eventName, descriptionLabel, eventDescription, buttons);
        Scene scene = new Scene(layout);
        scene.getStylesheets().add("main.css");
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }


}
