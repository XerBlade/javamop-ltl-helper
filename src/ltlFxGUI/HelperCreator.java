package ltlFxGUI;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import ltlAgentPkg.*;

import java.util.ArrayList;

/**
 * Created by Rex Cope
 * Create a helperLTL based on a formula created by parsing a string
 */
class HelperCreator {

    private static ArrayList<Event> events;     // Events list for current LTL being generated

    // Create helperLTL based on given Formula
    static HelperLTL getLTL(Formula ltlFormula, ArrayList<Event> eventsList)
                                        throws ParseErrorException {
        events = eventsList;

        HelperLTL ltl = new HelperLTL();    // LTL to convert from formula

        ltl.expression = getExpression(ltlFormula);

        return ltl;
    }

    // Create expression based on given formula
    private static Expression getExpression(Formula expFormula) throws ParseErrorException {
        Expression expression;      // Expression to convert from formula

        // Get the operator of the expression
        char operator = expFormula.getContent();

        switch (operator) {
            case 'p':
                expression = getSingleExp(expFormula.getName());
                break;
            case 't':
                expression = new LiteralExp(true);
                break;
            case 'f':
                expression = new LiteralExp(false);
                break;
            case 'V':
                expression = getUnaryExp("[]", expFormula.getSub1());
                break;
            case 'X':
                expression = getUnaryExp("()", expFormula.getSub1());
                break;
            case 'N':
                expression = getUnaryExp("!", expFormula.getSub1());
                break;
            case 'A':
                expression = getBinaryExp(expFormula.getSub1(), "&&", expFormula.getSub2());
                break;
            case 'O':
                expression = getBinaryExp(expFormula.getSub1(), "||", expFormula.getSub2());
                break;
            case 'U':
                expression = getBinaryExp(expFormula.getSub1(), "U", expFormula.getSub2());
                break;
            default:
                throw new ParseErrorException("unsupported operator");
        }

        return expression;
    }

    // create single expression
    private static SingleExp getSingleExp(String name) throws ParseErrorException {
        SingleExp expression = null;

        for (Event event : events)
        {
            if (name.equals(event.getName()))
            {
                expression = new SingleExp(event);
                break;
            }
        }

        if (expression == null)
        {
            throw new ParseErrorException("unsupported parameter: " + name);
        }

        return expression;
    }

    // Create unary expression
    private static UnaryOpExp getUnaryExp(String operator, Formula right) throws ParseErrorException {
        UnaryOpExp expression = new UnaryOpExp(operator);

        expression.right = getExpression(right);

        return expression;
    }

    // Create binary expression
    private static BinaryOpExp getBinaryExp(Formula left, String operator, Formula right) throws ParseErrorException {
        BinaryOpExp expression = new BinaryOpExp(operator);

        expression.left = getExpression(left);
        expression.right = getExpression(right);

        return expression;
    }
}
