package ltlFxGUI;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 *
 * Handles package lists for various parts of LTLAgents
 */
class PackageListHandler {

    // Returns comma separated list as string given collection of packages
    static String getPackagesString(Collection<String> packages) {
        String results = "";

        Iterator<String> itemIterator = packages.iterator();
        if (itemIterator.hasNext()) {
            // special-case first item.  in this case, no comma
            results = itemIterator.next();
            while (itemIterator.hasNext()) {
                results += "," + itemIterator.next();
            }
        }

        return results;
    }

    // Returns collection of packages given comma separated list
    static ConcurrentSkipListSet<String> getPackagesList(String packages) {
        ConcurrentSkipListSet<String> results = new ConcurrentSkipListSet<>(Arrays.asList(packages.split("\\s*,\\s*")));

        // Remove empty strings
        results.removeAll(Collections.singletonList(""));

        return results;
    }
}
