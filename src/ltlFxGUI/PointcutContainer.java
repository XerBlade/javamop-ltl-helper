package ltlFxGUI;

import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import ltlAgentPkg.*;
import ltlAgentPkg.Event;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Rex Cope
 */
class PointcutContainer extends BorderPane {
    private PointcutPane pointcutPane;  // Pointcut pane being displayed
    Event event;                  // Event this pointcut is defining
    private ArrayList<Parameter> parameters;        // Parameters in pointcut
    private MenuBar pointcutMenu;                // Pointcut represented as menus
    private Command adder;                  // Command to add new parameter
    private Command refresh;                // COmmand to refresh header label
    private Button eventEditorSave;        // Listens for null definitions
    private final String[] operators = {"!", "&&", "||", "(  )"};
    private final String[] expPrefixes = {"call", "execution", "get", "set", "handler", "staticinitialization", "initialization", "preinitialization", "condition"};
    private final String[] paramPrefixes = {"this", "target", "args"};

    PointcutContainer(Event event, ArrayList<Parameter> parameters, Command adder, Command refresh, Button eventEditorSave)
    {
        this.event = event;
        this.parameters = parameters;
        this.adder = adder;
        this.refresh = refresh;
        this.eventEditorSave = eventEditorSave;

        pointcutPane = new PointcutPane(event.pointcut, this.parameters, new MenuAction(), new GenerateAction(), adder);

        GenerateMenu();
        this.setCenter(pointcutMenu);
    }

    // Set the type of the expression
    private void setType(Object[] args)
    {
        String type = (String) args[0];
        Object paramNameOrContent = args[1];

        if(Arrays.asList(operators).contains(type))
        {
            event.setCutType(type);
        }
        else if (Arrays.asList(expPrefixes).contains(type)) {
            event.setCutType(type);
            ((ExpCut) event.pointcut).setContent((String) paramNameOrContent);
        }
        else
        {
            event.setCutType(type);

            ParamCut paramCut = (ParamCut) event.pointcut;
            paramCut.setParameter((Parameter) paramNameOrContent);
        }

        if(event.pointcut == null)
        {
            event.setCutType("param");
            ParamCut paramCut = (ParamCut) event.pointcut;
            paramCut.setParameter(parameters.get(0));
        }
        pointcutPane = new PointcutPane(event.pointcut, this.parameters, new MenuAction(), new GenerateAction(), adder);
        GenerateMenu();
        this.setCenter(pointcutMenu);
    }

    // Generates menu bar filled with event definition
    private void GenerateMenu()
    {
        ArrayList<Menu> menus = new ArrayList<>();  // list of menus
        pointcutMenu = new MenuBar();

        refresh.execute(null);

        getWhenMenu(menus);

        pointcutPane = new PointcutPane(event.pointcut, this.parameters, new MenuAction(), new GenerateAction(), adder);
        pointcutPane.getMenus(menus);

        pointcutMenu.getMenus().addAll(menus);
        this.setCenter(pointcutMenu);


        // If event currently has null parts, don't allow switching away for now
        if (event.hasNullExp())
            eventEditorSave.setDisable(true);
        else
            eventEditorSave.setDisable(false);
    }

    // Generates menu to select when event should run
    private void getWhenMenu(ArrayList<Menu> menus) {
        Menu whenMenu = new Menu();

        switch (event.getWhen()) {
            case "after returning": {
                String text = "after(";
                text += event.getParameterListRepresentation() + ")";
                text += " returning(";
                text += event.getAfterParameter().toLongString() + ") : ";
                whenMenu.setText(text);
                break;
            }
            case "after throwing": {
                String text = "after(";
                text += event.getParameterListRepresentation() + ")";
                text += " throwing(";
                text += event.getAfterParameter().toLongString() + ") : ";
                whenMenu.setText(text);
                break;
            }
            default: {
                String text = event.getWhen() + "(";
                text += event.getParameterListRepresentation();
                text += ") : ";
                whenMenu.setText(text);
                break;
            }
        }

        MenuItem beforeMenu = new MenuItem("before");
        MenuItem afterMenu = new MenuItem("after");
        Menu afterReturning = new Menu("after returning");
        Menu afterThrowing = new Menu("after throwing");

        beforeMenu.setOnAction(e -> {
            event.setWhen("before");
            GenerateMenu();
        });
        afterMenu.setOnAction(e -> {
            event.setWhen("after");
            GenerateMenu();
        });

        Menu staticParameters = new Menu("Static Parameters");
        for (Parameter parameter : parameters) {
            MenuItem paramItem = new MenuItem(parameter.toLongString());
            paramItem.setOnAction(e -> {
                event.setWhen("after returning");
                event.setAfterParameter(parameter);
                GenerateMenu();
            });
            staticParameters.getItems().addAll(paramItem);
        }
        if (!staticParameters.getItems().isEmpty()) {
            afterReturning.getItems().addAll(staticParameters);
        }
        MenuItem addParameter = new MenuItem("Add Parameter");
        addParameter.setOnAction(e -> {
            ArrayList<Object> args = new ArrayList<>();
            args.add(new AfterParamAction());
            args.add("after returning");
            callCommand(adder, args);
            event.setWhen("after returning");

            GenerateMenu();
        });
        afterReturning.getItems().addAll(addParameter);

        staticParameters = new Menu("Static Parameters");
        for (Parameter parameter : parameters) {
            MenuItem paramItem = new MenuItem(parameter.toLongString());
            paramItem.setOnAction(e -> {
                event.setWhen("after throwing");
                event.setAfterParameter(parameter);
                GenerateMenu();
            });
            staticParameters.getItems().addAll(paramItem);
        }
        if (!staticParameters.getItems().isEmpty()) {
            afterThrowing.getItems().addAll(staticParameters);
        }
        addParameter = new MenuItem("Add Parameter");
        addParameter.setOnAction(e -> {
            ArrayList<Object> args = new ArrayList<>();
            args.add(new AfterParamAction());
            args.add("after throwing");
            callCommand(adder, args);

            event.setWhen("after throwing");
            GenerateMenu();
        });
        afterThrowing.getItems().addAll(addParameter);

        whenMenu.getItems().addAll(beforeMenu, afterMenu, afterReturning, afterThrowing);

        menus.add(whenMenu);
    }

    // Execute command passed in with passed data as parameter
    private static void callCommand(Command command, Object data)
    {
        command.execute(data);
    }

    private class MenuAction implements Command
    {
        @Override
        public void execute(Object data)
        {
            String actionPart = (String) ((Object[]) data)[0];
            String[] outData;
            if (Arrays.asList(paramPrefixes).contains(actionPart)) {
                Parameter parameter = (Parameter) ((Object[]) data)[1];

                outData = new String[]{(String) ((Object[]) data)[0], parameter.getName()};
            } else {
                outData = new String[]{(String) ((Object[]) data)[0], (String) ((Object[]) data)[1]};
            }
            setType(outData);
        }
    }

    private class AfterParamAction implements Command {
        @Override
        public void execute(Object data) {
            Parameter parameter = (Parameter) ((Object[]) data)[1];
            event.setAfterParameter(parameter);
        }
    }

    private class GenerateAction implements Command
    {

        @Override
        public void execute(Object data) {
            GenerateMenu();
        }
    }
}
