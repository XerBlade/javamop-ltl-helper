package ltlFxGUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ltlAgentPkg.AInstanceVar;

/**
 * Created by Rex Cope
 */
class InstanceVarEditor {
    private static String answer;  // Whether user clicked OK
    private static Boolean typeIsEmpty, nameIsEmpty;

    static String display(AInstanceVar variable) {

        Stage window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);
        window.setTitle("Aspect Instance Variable Editor");

        Label typeLabel = new Label("Type");
        TextField variableType = new TextField();
        variableType.setText(variable.getType());
        typeIsEmpty = variableType.getText().trim().isEmpty();

        Label nameLabel = new Label("Name");
        TextField variableName = new TextField();
        variableName.setText(variable.getId());
        nameIsEmpty = variableName.getText().trim().isEmpty();

        Label initializerLabel = new Label("Initializer (optional)");
        TextField variableInitializer = new TextField();
        variableInitializer.setText(variable.getInitializer());

        Label packageLabel = new Label("Package (if needed)");
        TextField variablePackage = new TextField();
        variablePackage.setText(variable.getRequiredPackage());

        Label descriptionLabel = new Label("Description");
        TextArea variableDescription = new TextArea();
        variableDescription.setText(variable.getDescription());

        Button okButton = new Button("Save");
        Button removeButton = new Button("Remove");

        okButton.setOnAction(e -> {
            variable.setType(variableType.getText());
            variable.setId(variableName.getText());
            variable.setInitializer(variableInitializer.getText());
            variable.setRequiredPackage(variablePackage.getText());
            variable.setDescription(variableDescription.getText());
            answer = "Save";
            window.close();
        });

        removeButton.setOnAction(e -> {
            answer = "Remove";
            window.close();
        });

        window.setOnCloseRequest(we -> answer = "Cancel");

        VBox layout = new VBox(10);
        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(okButton, removeButton);

        // Enable/Disable save button depending on whether something is entered
        okButton.setDisable(typeIsEmpty || nameIsEmpty);

        // Do some validation
        variableType.textProperty().addListener((observable, oldValue, newValue) -> {
            typeIsEmpty = newValue.trim().isEmpty();
            okButton.setDisable(typeIsEmpty || nameIsEmpty);
        });
        variableName.textProperty().addListener((observable, oldValue, newValue) -> {
            nameIsEmpty = newValue.trim().isEmpty();
            okButton.setDisable(typeIsEmpty || nameIsEmpty);
        });

        // Add everything
        layout.getChildren().addAll(typeLabel, variableType, nameLabel, variableName, initializerLabel, variableInitializer, packageLabel, variablePackage, descriptionLabel, variableDescription, buttons);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }
}

