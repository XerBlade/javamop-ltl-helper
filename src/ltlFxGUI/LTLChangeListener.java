package ltlFxGUI;

/**
 * Created by Rex Cope
 */
interface LTLChangeListener {
    void onChanged();
}
