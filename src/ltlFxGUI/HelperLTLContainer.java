package ltlFxGUI;

import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import ltlAgentPkg.*;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Rex Cope
 */
class HelperLTLContainer extends BorderPane {
    private ExpressionPane expressionPane;  // Expression pane being displayed
    private HelperLTL ltl;                  // LTL being represented
    private ArrayList<Event> events;        // Events in LTL
    private MenuBar ltlMenu;                // LTL represented as menus
    private Command adder;                  // Command to add new event
    private LTLChangeListener modifiedListener; // Listener for changes to file
    private final String[] supportedOperators = {"!", "[]", "<>", "o", "[*]", "<*>", "(*)", "&&", "||", "^", "=>", "<=>", "U", "S", "(  )"};

    HelperLTLContainer(HelperLTL ltl, ArrayList<Event> events, Command adder, LTLChangeListener listener)
    {
        this.ltl = ltl;
        this.events = events;
        this.adder = adder;
        this.modifiedListener = listener;

        expressionPane = new ExpressionPane(ltl.expression, this.events, new MenuAction(), new GenerateAction(), adder, modifiedListener);

        GenerateMenu();
        this.setCenter(ltlMenu);
    }

    // Set the type of the expression
    private void setType(String type)
    {

        if(Arrays.asList(supportedOperators).contains(type))
        {
            ltl.setExpType(type);
        }
        else if (type.equals("true") || type.equals("false")) {
            ltl.setExpType(type);
        }
        else
        {
            ltl.setExpType("single");
            for (Event event : events)
            {
                if (type.equals(event.getName()))
                {
                    ltl.expression = new SingleExp(event);
                    break;
                }
            }
        }

        if(ltl.expression == null)
        {
            ltl.setExpType("single");
            SingleExp singleExp = (SingleExp) ltl.expression;
            singleExp.setEvent(events.get(0));
        }
        expressionPane = new ExpressionPane(ltl.expression, this.events, new MenuAction(), new GenerateAction(), adder, modifiedListener);
        GenerateMenu();
        this.setCenter(ltlMenu);
    }

    // Generates menu bar filled with LTL
    private void GenerateMenu()
    {
        ArrayList<Menu> menus = new ArrayList<>();  // list of menus
        ltlMenu = new MenuBar();

        expressionPane.getMenus(menus);

        ltlMenu.getMenus().addAll(menus);
        this.setCenter(ltlMenu);

    }

    private class MenuAction implements Command
    {
        @Override
        public void execute(Object data)
        {
            setType((String) data);
        }
    }

    private class GenerateAction implements Command
    {

        @Override
        public void execute(Object data) {
            GenerateMenu();
        }
    }
}
