package ltlFxGUI;

import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import ltlAgentPkg.AInstanceVar;

/**
 * Created by Rex Cope
 */
class InstanceVarButton extends StackPane {
    private Button variableButton;     // Actual button to edit instanceVar
    private AInstanceVar instanceVar;            // Variable represented

    InstanceVarButton(AInstanceVar instanceVar) {
        this.instanceVar = instanceVar;

        variableButton = new Button(instanceVar.getType() + " " + instanceVar.getId());

        this.getChildren().add(variableButton);
    }

    Button getVariableButton() {
        return this.variableButton;
    }

    public AInstanceVar getInstanceVar() {
        return this.instanceVar;
    }
}
