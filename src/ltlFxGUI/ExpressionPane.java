package ltlFxGUI;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import ltlAgentPkg.*;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Rex Cope
 */
class ExpressionPane {

    private ExpressionPane left;    // left hand side of expression
    private ExpressionPane right;   // right hand side of expression
    private ArrayList<Event> events;    // Events in this LTL
    private Expression expression;      // Expression represented
    private String type;                // Type of expression
    private Command generate;           // Action to force generation of menus
    private Command adder;              // Action to add a new event
    private Menu typeSelect;            // Menu to select type
    private LTLChangeListener modifiedListener; // Listener for changes to file
    private final String[] operators = {"!", "[]", "<>", "o", "[*]", "<*>", "(*)", "&&", "||", "^", "=>", "<=>", "U", "S", "(  )"};


    ExpressionPane(Expression expression, ArrayList<Event> events, Command action, Command generate, Command adder, LTLChangeListener listener)
    {
        this.expression = expression;
        this.events = events;
        this.generate = generate;
        this.adder = adder;
        this.modifiedListener = listener;

        typeSelect = new Menu();
        // Initialize selection
        if (expression != null)
        {
            switch (expression.getType()) {
                case "single":
                    type = "single";
                    typeSelect.setText(((SingleExp) expression).getEvent().getName());
                    break;
                case "literal":
                    type = "literal";
                    if (((LiteralExp) expression).getValue()) {
                        typeSelect.setText("true");
                    } else {
                        typeSelect.setText("false");
                    }

                    break;
                default:
                    if (((OpExp) expression).getOperator().equals("(  )")) {
                        typeSelect.setText("(");
                    }
                    else {
                        typeSelect.setText(((OpExp) expression).getOperator());
                    }
                    createOpExpPanes(expression.getType());
                    break;
            }
        }
        else
        {
            type = "null";
            typeSelect.setText("-select-");
            typeSelect.setId("nullExpMenu");
        }

        // Create menu
        Menu eventMenu = new Menu("Predicates");
        Menu operatorMenu = new Menu("Operators");
        Menu literalMenu = new Menu("Literals");

        for (Event event : events)
        {
            MenuItem eventItem = new MenuItem(event.getName());
            eventItem.setOnAction(e -> {
                callCommand(action, event.getName());
                modifiedListener.onChanged();
            });
            eventMenu.getItems().addAll(eventItem);
        }

        MenuItem addEvent = new MenuItem("Add Event");
        addEvent.setOnAction(e -> {
            callCommand(adder, action);
            modifiedListener.onChanged();
        });
        eventMenu.getItems().addAll(new SeparatorMenuItem(), addEvent);

        for (String operator : operators)
        {
            MenuItem operatorItem = new MenuItem(operator);
            operatorItem.setOnAction(e -> {
                callCommand(action, operator);
                modifiedListener.onChanged();
            });
            operatorMenu.getItems().addAll(operatorItem);
        }

        MenuItem trueItem = new MenuItem("true");
        trueItem.setOnAction(e -> {
            callCommand(action, "true");
            modifiedListener.onChanged();
        });
        MenuItem falseItem = new MenuItem("false");
        falseItem.setOnAction(e -> {
            callCommand(action, "false");
            modifiedListener.onChanged();
        });
        literalMenu.getItems().addAll(trueItem, falseItem);

        typeSelect.getItems().addAll(eventMenu, operatorMenu, literalMenu);
    }

    // Creates left and right panes according to type
    private void createOpExpPanes(String type)
    {
        switch (type) {
            case "unary":
                this.type = "unary";

                right = new ExpressionPane(((UnaryOpExp) expression).right, events, new RightAction(), generate, adder, modifiedListener);

                break;
            case "binary":
                this.type = "binary";

                left = new ExpressionPane(((BinaryOpExp) expression).left, events, new LeftAction(), generate, adder, modifiedListener);

                right = new ExpressionPane(((BinaryOpExp) expression).right, events, new RightAction(), generate, adder, modifiedListener);

                break;
            case "paren":
                this.type = "paren";

                right = new ExpressionPane(((ParenExp) expression).inner, events, new RightAction(), generate, adder, modifiedListener);
                break;
        }
    }

    // Creates the type of expression based on selection
    private void setLeftType(String type)
    {

        if (Arrays.asList(operators).contains(type))
        {
            setLeftTypeOperator((BinaryOpExp) expression, type);
        }
        else if (type.equals("true") || type.equals("false")) {
            setLeftTypeLiteral((BinaryOpExp) expression, type);
        }
        else {
            setLeftTypeSingle((BinaryOpExp) expression, type);
        }

        callCommand(generate, null);
    }

    private void setLeftTypeLiteral(BinaryOpExp expression, String type) {
        expression.setLeftType(type);

        left = new ExpressionPane(expression.left, events, new LeftAction(), generate, adder, modifiedListener);
    }

    private void setLeftTypeOperator(BinaryOpExp expression, String type)
    {
        expression.setLeftType(type);

        left = new ExpressionPane(expression.left, events, new LeftAction(), generate, adder, modifiedListener);

    }

    private void setLeftTypeSingle(BinaryOpExp expression, String type)
    {
        expression.setLeftType("single");
        SingleExp singleExp = ((SingleExp) expression.left);
        singleExp.setEvent(events.get(0));

        for (Event event : events)
        {
            if (type.equals(event.getName()))
            {

                singleExp.setEvent(event);

                left = new ExpressionPane(singleExp, events, new LeftAction(), generate, adder, modifiedListener);

                break;
            }
        }
    }

    // Creates the type of expression based on selection
    private void setRightType(String type)
    {
        Expression rightExpression;

        switch (this.type) {
            case "unary":
                if (Arrays.asList(operators).contains(type)) {
                    ((UnaryOpExp) expression).setRightType(type);
                } else if (type.equals("true") || type.equals("false")) {
                    ((UnaryOpExp) expression).setRightType(type);
                } else {
                    ((UnaryOpExp) expression).setRightType("single");
                }
                rightExpression = ((UnaryOpExp) expression).right;
                break;
            case "paren":
                if (Arrays.asList(operators).contains(type)) {
                    ((ParenExp) expression).setInnerType(type);
                } else if (type.equals("true") || type.equals("false")) {
                    ((ParenExp) expression).setInnerType(type);
                } else {
                    ((ParenExp) expression).setInnerType("single");
                }
                rightExpression = ((ParenExp) expression).inner;
                break;
            default:
                if (Arrays.asList(operators).contains(type)) {
                    ((BinaryOpExp) expression).setRightType(type);
                } else if (type.equals("true") || type.equals("false")) {
                    ((BinaryOpExp) expression).setRightType(type);
                } else {
                    ((BinaryOpExp) expression).setRightType("single");
                }
                rightExpression = ((BinaryOpExp) expression).right;
                break;
        }

        if (Arrays.asList(operators).contains(type))
        {
            right = new ExpressionPane(rightExpression, events, new RightAction(), generate, adder, modifiedListener);

        }
        else if (type.equals("true") || type.equals("false")) {
            right = new ExpressionPane(rightExpression, events, new RightAction(), generate, adder, modifiedListener);
        }
        else
        {
            for (Event event : events)
            {
                if (type.equals(event.getName()))
                {
                    SingleExp singleExp = (SingleExp) rightExpression;
                    singleExp.setEvent(event);

                    right = new ExpressionPane(singleExp, events, new RightAction(), generate, adder, modifiedListener);

                    break;
                }
            }
        }

        callCommand(generate, null);
    }


    void getMenus(ArrayList<Menu> menus)
    {
        switch (type)
        {
            case "single":
            case "literal":
            case "null":
                menus.add(typeSelect);
                break;
            case "unary":
                menus.add(typeSelect);
                right.getMenus(menus);
                break;
            case "binary":
                left.getMenus(menus);
                menus.add(typeSelect);
                right.getMenus(menus);
                break;
            case "paren":
                menus.add(typeSelect);
                right.getMenus(menus);
                menus.add(new Menu(")"));
                break;
        }
    }

    // Execute command passed in with passed data as parameter
    private static void callCommand(Command command, Object data)
    {
        command.execute(data);
    }

    private class LeftAction implements Command
    {
        @Override
        public void execute(Object data)
        {
            setLeftType((String) data);
        }
    }

    private class RightAction implements Command
    {
        @Override
        public void execute(Object data)
        {
            setRightType((String) data);
        }
    }
}
