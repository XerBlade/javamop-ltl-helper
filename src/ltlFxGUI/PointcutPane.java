package ltlFxGUI;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import ltlAgentPkg.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Rex Cope
 */
class PointcutPane {
    private PointcutPane left;    // left hand side of expression
    private PointcutPane right;   // right hand side of expression
    private ArrayList<Parameter> parameters;    // Parameters in this pointcut
    public Pointcut pointcut;      // Expression represented
    private String type;                // Type of expression
    private Command generate;           // Action to force generation of menus
    private Command adder;              // Action to add a new parameter
    private Menu typeSelect;            // Menu to select type
    private final String[] operators = {"!", "&&", "||", "(  )"};
    private final String[] expPrefixes = {"call", "execution", "get", "set", "handler", "staticinitialization", "initialization", "preinitialization", "condition"};
    private final String[] paramPrefixes = {"this", "target", "args"};

    PointcutPane(Pointcut pointcut, ArrayList<Parameter> parameters, Command action, Command generate, Command adder)
    {
        this.pointcut = pointcut;
        this.parameters = parameters;
        this.generate = generate;
        this.adder = adder;

        typeSelect = new Menu();
        // Initialize selection
        if (pointcut != null)
        {
            switch (pointcut.getType()) {
                case "exp":
                    type = ((ExpCut) pointcut).getPrefix();
                    typeSelect.setText(pointcut.toLongString());
                    break;
                case "param":
                    type = ((ParamCut) pointcut).getPrefix();
                    typeSelect.setText(pointcut.toLongString());

                    break;
                default:
                    if (((OpCut) pointcut).getOperator().equals("(  )")) {
                        typeSelect.setText("(");
                    }
                    else {
                        typeSelect.setText(((OpCut) pointcut).getOperator());
                    }
                    createOpCutPanes(pointcut.getType());
                    break;
            }
        }
        else
        {
            type = "null";
            typeSelect.setText("-select-");
            typeSelect.setId("nullExpMenu");
        }

        // Create menu
        Menu expMenu = new Menu("Expression based");
        Menu paramMenu = new Menu("Parameter based");
        Menu operatorMenu = new Menu("Operator");

        ArrayList<Menu> paramSubMenus = new ArrayList<>();
        for (String prefix : paramPrefixes)
            paramSubMenus.add(new Menu(prefix));

        ArrayList<MenuItem> expSubMenus = new ArrayList<>();
        for (String prefix : expPrefixes)
            expSubMenus.add(new MenuItem(prefix));

        for (MenuItem menuitem : expSubMenus) {
            menuitem.setOnAction(e -> {
                String content = "";

                TextInputDialog dialog = new TextInputDialog();
                dialog.setTitle("Pointcut Signature");
                dialog.setHeaderText("Enter the signature for this pointcut");
                dialog.setContentText(menuitem.getText() + "(");

                // Traditional way to get the response value.
                Optional<String> result = dialog.showAndWait();
                if (result.isPresent()){
                    content = result.get();
                }


                String[] args = {menuitem.getText(), content};
                callCommand(action, args);
            });
        }

        for (Menu menu : paramSubMenus) {
            Menu staticParameters = new Menu("Static Parameters");
            for (Parameter parameter : parameters) {
                MenuItem paramItem = new MenuItem(parameter.toLongString());
                paramItem.setOnAction(e -> {
                    Object[] args = {menu.getText(), parameter};
                    callCommand(action, args);
                });
                staticParameters.getItems().addAll(paramItem);
            }
            if (!staticParameters.getItems().isEmpty()) {
                menu.getItems().addAll(staticParameters);
            }
            MenuItem addParameter = new MenuItem("Add Parameter");
            addParameter.setOnAction(e -> {
                ArrayList<Object> args = new ArrayList<>();
                args.add(action);
                args.add(menu.getText());
                callCommand(adder, args);
            });
            menu.getItems().addAll(addParameter);
        }

        for (String operator : operators)
        {
            MenuItem operatorItem = new MenuItem(operator);
            operatorItem.setOnAction(e -> {
                String[] args = {operator, null};
                callCommand(action, args);
            });
            operatorMenu.getItems().addAll(operatorItem);
        }

        expMenu.getItems().addAll(expSubMenus);
        paramMenu.getItems().addAll(paramSubMenus);

        typeSelect.getItems().addAll(expMenu, paramMenu, operatorMenu);
    }

    // Creates left and right panes according to type
    private void createOpCutPanes(String type)
    {
        switch (type) {
            case "unary":
                this.type = "unary";
                right = new PointcutPane(((UnaryOpCut) pointcut).right, parameters, new RightAction(), generate, adder);
                break;
            case "binary":
                this.type = "binary";
                left = new PointcutPane(((BinaryOpCut) pointcut).left, parameters, new LeftAction(), generate, adder);
                right = new PointcutPane(((BinaryOpCut) pointcut).right, parameters, new RightAction(), generate, adder);
                break;
            case "paren":
                this.type = "paren";
                right = new PointcutPane(((ParenCut) pointcut).inner, parameters, new RightAction(), generate, adder);
                break;
        }
    }

    // Creates the type of expression based on selection
    private void setLeftType(Object[] args)
    {
        String type = (String) args[0];
        Object paramName = args[1];

        if (Arrays.asList(operators).contains(type))
        {
            setLeftTypeOperator((BinaryOpCut) pointcut, type);
        }
        else if (Arrays.asList(expPrefixes).contains(type)) {
            setLeftTypeExp((BinaryOpCut) pointcut, type, (String) paramName);
        }
        else {
            setLeftTypeParam((BinaryOpCut) pointcut, type, (Parameter) paramName);
        }

        callCommand(generate, null);
    }

    private void setLeftTypeOperator(BinaryOpCut pointcut, String type)
    {
        pointcut.setLeftType(type);

        left = new PointcutPane(pointcut.left, parameters, new LeftAction(), generate, adder);
    }

    private void setLeftTypeExp(BinaryOpCut pointcut, String type, String content)
    {
        pointcut.setLeftType(type);
        ExpCut expCut = ((ExpCut) pointcut.left);
        expCut.setContent(content);
        left = new PointcutPane(expCut, parameters, new LeftAction(), generate, adder);
    }

    private void setLeftTypeParam(BinaryOpCut pointcut, String type, Parameter parameter)
    {
        pointcut.setLeftType(type);
        ParamCut paramCut = ((ParamCut) pointcut.left);



                paramCut.setParameter(parameter);

                left = new PointcutPane(paramCut, parameters, new LeftAction(), generate, adder);

    }

    // Creates the type of expression based on selection
    private void setRightType(Object[] args)
    {
        String type = (String) args[0];
        Object paramName = args[1];

        Pointcut rightPointcut;

        switch (this.type) {
            case "unary":
                if (Arrays.asList(operators).contains(type)) {
                    ((UnaryOpCut) pointcut).setRightType(type);
                } else if (Arrays.asList(expPrefixes).contains(type)) {
                    ((UnaryOpCut) pointcut).setRightType(type);
                } else if (Arrays.asList(paramPrefixes).contains(type)) {
                    ((UnaryOpCut) pointcut).setRightType(type);
                }
                rightPointcut = ((UnaryOpCut) pointcut).right;
                break;
            case "binary":
                if (Arrays.asList(operators).contains(type)) {
                    ((BinaryOpCut) pointcut).setRightType(type);
                } else if (Arrays.asList(expPrefixes).contains(type)) {
                    ((BinaryOpCut) pointcut).setRightType(type);
                } else if (Arrays.asList(paramPrefixes).contains(type)) {
                    ((BinaryOpCut) pointcut).setRightType(type);
                }
                rightPointcut = ((BinaryOpCut) pointcut).right;
                break;
            default:
                if (Arrays.asList(operators).contains(type)) {
                    ((ParenCut) pointcut).setInnerType(type);
                } else if (Arrays.asList(expPrefixes).contains(type)) {
                    ((ParenCut) pointcut).setInnerType(type);
                } else if (Arrays.asList(paramPrefixes).contains(type)) {
                    ((ParenCut) pointcut).setInnerType(type);
                }
                rightPointcut = ((ParenCut) pointcut).inner;
                break;
        }

        if (Arrays.asList(operators).contains(type))
        {
            right = new PointcutPane(rightPointcut, parameters, new RightAction(), generate, adder);
        }
        else if (Arrays.asList(expPrefixes).contains(type)) {

            ExpCut expCut = ((ExpCut) rightPointcut);
            expCut.setContent((String) paramName);
            right = new PointcutPane(rightPointcut, parameters, new RightAction(), generate, adder);
        }
        else if (Arrays.asList(paramPrefixes).contains(type))
        {

                    ParamCut paramCut = (ParamCut) rightPointcut;
                    paramCut.setParameter((Parameter) paramName);

                    right = new PointcutPane(paramCut, parameters, new RightAction(), generate, adder);

        }

        callCommand(generate, null);
    }


    void getMenus(ArrayList<Menu> menus)
    {
        if (pointcut == null)
            menus.add(typeSelect);
        else {
            switch (pointcut.getType()) {
                case "exp":
                case "param":
                    menus.add(typeSelect);
                    break;
                case "unary":
                    menus.add(typeSelect);
                    right.getMenus(menus);
                    break;
                case "binary":
                    left.getMenus(menus);
                    menus.add(typeSelect);
                    right.getMenus(menus);
                    break;
                case "paren":
                    menus.add(typeSelect);
                    right.getMenus(menus);
                    menus.add(new Menu(")"));
                    break;
            }
        }
    }

    // Execute command passed in with passed data as parameter
    private static void callCommand(Command command, Object data)
    {
        command.execute(data);
    }




    private class LeftAction implements Command
    {
        @Override
        public void execute(Object data)
        {
            String actionPart = (String) ((Object[]) data)[0];
            Object[] outData;
            if (Arrays.asList(paramPrefixes).contains(actionPart)) {
                Parameter parameter = (Parameter) ((Object[]) data)[1];

                outData = new Object[]{((Object[]) data)[0], parameter};
            } else {
                outData = new String[]{(String) ((Object[]) data)[0], (String) ((Object[]) data)[1]};
            }
            setLeftType(outData);
        }
    }

    private class RightAction implements Command
    {
        @Override
        public void execute(Object data)
        {
            String actionPart = (String) ((Object[]) data)[0];
            Object[] outData;
            if (Arrays.asList(paramPrefixes).contains(actionPart)) {
                Parameter parameter = (Parameter) ((Object[]) data)[1];

                outData = new Object[]{((Object[]) data)[0], parameter};
            } else {
                outData = new String[]{(String) ((Object[]) data)[0], (String) ((Object[]) data)[1]};
            }
            setRightType(outData);
        }
    }
}
