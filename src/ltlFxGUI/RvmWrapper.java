package ltlFxGUI;

import java.io.*;

/**
 * Created by Rex Cope
 */
class RvmWrapper {

    // Run Rv-Monitor with single rvm file
    static String runRvm(File inputRvmFile, File workingDirectory) throws IOException {
        String messagesFromRunning = "";

        Process process;


        String rvmStringToRun;
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            rvmStringToRun = "rv-monitor.bat";
        } else {
            rvmStringToRun = "rv-monitor";
        }


        // Get relative paths to file and output folder
        String relativePathToRvmFile = workingDirectory.toPath().relativize(inputRvmFile.toPath()).toString();

        messagesFromRunning += "> " + rvmStringToRun + " " + "\"" + relativePathToRvmFile + "\"" + "\n";

        ProcessBuilder builder = new ProcessBuilder(
                rvmStringToRun, relativePathToRvmFile);
        builder.directory(workingDirectory);
        builder.redirectErrorStream(true);
        process = builder.start();


        InputStream inputStream = process.getInputStream();
        InputStream errorInputStream = process.getErrorStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        InputStreamReader errorInputStreamReader = new InputStreamReader(errorInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        BufferedReader errorBufferedReader = new BufferedReader(errorInputStreamReader);
        String line;


        while ((line = bufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        while ((line = errorBufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        messagesFromRunning += "\n";

        System.out.println(messagesFromRunning);


        return messagesFromRunning;
    }

}
