package ltlFxGUI;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import ltlAgentPkg.*;
import ltlAgentPkg.ioLML.LMLIO;
import org.jdom2.JDOMException;

import java.io.*;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 */
public class Controller implements Initializable, LTLChangeListener {
    public Button openFolderButton;
    public Button newFileButton;
    public Button newFolderButton;
    public Button saveAllButton;
    public Button newLTLButton;
    public Button removeLTLButton;
    public BorderPane localEventPane;
    public VBox ltlCreationPane;
    public MenuItem saveCurrentMenu;
    public Button saveCurrentButton;
    public MenuItem discardMenu;
    public Button discardButton;
    public Button javaMopButton;
    public BorderPane parameterPane;
    public HBox parameterListDisplay;
    public Button addParameterButton;
    public ComboBox<Event> eventSelectorBox;
    public Button setEventMappingButton;
    public BorderPane eventMappingPane;
    public BorderPane propertyHandlerPane;
    public ComboBox<String> passFailSelectorBox;
    public TextArea propertyHandlerText;
    public Button generateMopFileButton;
    public Button runJavaMopButton;
    public TextField propertyHandlerPackagesField;
    public ComboBox<String> modifierSelectorBox;
    public BorderPane variablePane;
    public HBox variableListDisplay;
    public Button addVariableButton;
    @FXML private MenuItem newFolderMenu;
    @FXML private TreeView<String> treeView;
    @FXML private Tab customTab;
    @FXML private Tab helperTab;
    @FXML private TabPane tabPane;
    @FXML private MenuItem newMenu;
    @FXML private MenuItem addMenu;
    @FXML private MenuItem removeMenu;
    @FXML private HBox eventsListDisplay;
    @FXML private Button addEventButton;
    @FXML private ScrollPane ltlDisplay;
    @FXML private TextArea descriptionText;
    private ArrayList<LTLFile> ltlFiles;
    private LTLFile selectedFile;
    private LTLAgent selectedLTL;
    private FilePathTreeItem selectedFileNode;
    private FilePathTreeItem selectedLTLNode;
    private FilePathTreeItem selectedDirNode;
    private Path rootPath;         // Path to root folder
    @SuppressWarnings("unused")
    private boolean saved;          // Whether has been saved to a file
    private File inputFile;                 // input file for javamop launcher
    private File outputFile;                // output file for javamop launcher

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        ltlFiles = new ArrayList<>();


        // Get path to current directory
        Path rootLocation = Paths.get(System.getProperty("user.dir"));
        InitializeTree(rootLocation);

        // ltlName.textProperty().addListener((observable, oldValue, newValue) -> ltlNameChanged());
        descriptionText.textProperty().addListener((observable, oldValue, newValue) -> descriptionChanged());
        propertyHandlerText.textProperty().addListener((observable, oldValue, newValue) -> propertyHandlerChanged());
        propertyHandlerPackagesField.textProperty().addListener((observable, oldValue, newValue) -> propertyHandlerPackagesChanged());

        parameterPane.setVisible(false);
        variablePane.setVisible(false);
        eventMappingPane.setVisible(false);
        localEventPane.setVisible(false);
        ltlCreationPane.setVisible(false);
        propertyHandlerPane.setVisible(false);
        descriptionText.setVisible(false);
        modifierSelectorBox.setVisible(false);
        tabPane.setVisible(false);

        // Set up event combo box behavior
        eventSelectorBox.setCellFactory(
            new Callback<ListView<Event>, ListCell<Event>>() {
                @Override public ListCell<Event> call(ListView<Event> param) {
                    return new ListCell<Event>() {
                        {
                            super.setPrefWidth(100);
                        }
                        @Override public void updateItem(Event item,
                                                         boolean empty) {
                            super.updateItem(item, empty);
                            if (item != null) {
                                setText(item.getName());
                                if (item.pointcut == null) {
                                    setTextFill(Color.RED);
                                }
                                else {
                                    setTextFill(Color.BLACK);
                                }
                            }
                            else {
                                setText(null);
                            }
                        }
                    };
                }
            });

        // Set up validation/violation combo box
        passFailSelectorBox.getItems().addAll("@violation", "@validation");

        // Set up modifier combo box
        modifierSelectorBox.getItems().addAll("standard", "full-binding", "connected", "decentralized");

        // Prompt user for root folder
        openMenuClicked();
    }

    private void propertyHandlerPackagesChanged() {
        String newHandlerPackages = propertyHandlerPackagesField.getText();
        FilePathTreeItem selectedItem = (FilePathTreeItem) treeView.getSelectionModel().getSelectedItem();

        if (selectedItem.isLTL()) {


            if (!(PackageListHandler.getPackagesString(selectedLTL.getPropertyPackages()).equals(newHandlerPackages))) {
                ConcurrentSkipListSet<String> newPackagesList = PackageListHandler.getPackagesList(newHandlerPackages);
                selectedLTL.getPropertyPackages().clear();
                selectedLTL.getPropertyPackages().addAll(newPackagesList);
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        }
    }

    private void InitializeTree(Path rootLocation) {

        ltlFiles.clear();
        FilePathTreeItem rootNode;  // Root node of the tree view


        rootNode = getDirNodeAtPath(rootLocation);
        rootNode.setExpanded(true);

        rootPath = rootLocation;

        saved = true;

        treeView.setCellFactory(p -> new TreeCellImpl());

        treeView.setRoot(rootNode);


        // Set selection event on tree
        treeView.setOnMouseClicked(e -> {
            FilePathTreeItem selectedItem = (FilePathTreeItem) treeView.getSelectionModel().getSelectedItem();
            if (selectedItem.isLTL()) {


                selectedFile = selectedItem.getLTLFile();
                selectedLTL = selectedItem.getLtl();
                selectedFileNode = (FilePathTreeItem) selectedItem.getParent();
                selectedLTLNode = selectedItem;
                selectedDirNode = (FilePathTreeItem) selectedFileNode.getParent();

                addMenu.setDisable(false);
                newLTLButton.setDisable(false);
                removeMenu.setDisable(false);
                removeLTLButton.setDisable(false);
                newMenu.setDisable(true);
                newFileButton.setDisable(true);
                newFolderMenu.setDisable(true);
                newFolderButton.setDisable(true);
                saveCurrentMenu.setDisable(false);
                saveCurrentButton.setDisable(false);
                discardMenu.setDisable(false);
                discardButton.setDisable(false);
                generateMopFileButton.setDisable(false);
                runJavaMopButton.setDisable(false);

                changeEditLTL();
            } else if (selectedItem.isFile()) {
                selectedFile = selectedItem.getLTLFile();
                selectedLTL = null;
                selectedFileNode = selectedItem;
                selectedLTLNode = null;
                selectedDirNode = (FilePathTreeItem) selectedFileNode.getParent();

                addMenu.setDisable(false);
                newLTLButton.setDisable(false);
                removeMenu.setDisable(true);
                removeLTLButton.setDisable(true);
                newMenu.setDisable(true);
                newFileButton.setDisable(true);
                newFolderMenu.setDisable(true);
                newFolderButton.setDisable(true);
                saveCurrentMenu.setDisable(false);
                saveCurrentButton.setDisable(false);
                discardMenu.setDisable(false);
                discardButton.setDisable(false);
                generateMopFileButton.setDisable(true);
                runJavaMopButton.setDisable(false);

                changeEditLTL();
            } else if (selectedItem.isDirectory()) {
                selectedFile = null;
                selectedLTL = null;
                selectedFileNode = null;
                selectedLTLNode = null;
                selectedDirNode = selectedItem;

                addMenu.setDisable(true);
                newLTLButton.setDisable(true);
                removeMenu.setDisable(true);
                removeLTLButton.setDisable(true);
                newMenu.setDisable(false);
                newFileButton.setDisable(false);
                newFolderMenu.setDisable(false);
                newFolderButton.setDisable(false);
                saveCurrentMenu.setDisable(true);
                saveCurrentButton.setDisable(true);
                discardMenu.setDisable(true);
                discardButton.setDisable(true);
                generateMopFileButton.setDisable(true);
                runJavaMopButton.setDisable(true);

                changeEditLTL();
            }
        });

        addMenu.setDisable(true);
        newLTLButton.setDisable(true);
        removeMenu.setDisable(true);
        removeLTLButton.setDisable(true);
        newMenu.setDisable(true);
        newFileButton.setDisable(true);
        newFolderMenu.setDisable(true);
        newFolderButton.setDisable(true);
        saveCurrentMenu.setDisable(true);
        saveCurrentButton.setDisable(true);
        discardMenu.setDisable(true);
        discardButton.setDisable(true);
        generateMopFileButton.setDisable(true);
        runJavaMopButton.setDisable(true);
    }

    private void generateMopFile() {
        if (selectedFile != null && selectedLTL != null) {
            LTLAgent currentLTL = selectedLTL;
            File mopFile;

            try {
                System.out.println("Loading " + selectedLTL.getName());

                // Pick mop file to generate
                FileChooser fileChooser = new FileChooser();


                fileChooser.setTitle("Select File Name");

                fileChooser.setInitialDirectory(rootPath.toFile());


                //Set extension filter
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("MOP Files", "*.mop");
                fileChooser.getExtensionFilters().add(extFilter);




                //Show open file dialog
                File file = fileChooser.showSaveDialog(null);

                if(file!=null) {
                    mopFile = file;
                }
                else {
                    return;
                }


                System.out.println("Generating sendrectest.mop");

                // Get package for the LTL
                String mopPackage = getMopPackage(selectedFileNode);
                currentLTL.setMopPackage(mopPackage);

                String content = currentLTL.toLongString(selectedFile.getGlobalEvents());


                // if file doesn't exists, then create it
                if (!mopFile.exists()) {
                    boolean created = mopFile.createNewFile();
                    if (!created)
                        throw new IOException("Failed to create mop file");
                }

                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.close();

                System.out.println("Done");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void discardMenuClick() {
        // Get current file and LTL
        LTLAgent currentLTL = selectedLTL;
        LTLFile currentFile = selectedFile;

        // Confirm action
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.setTitle("Confirm Discard");
        alert.setHeaderText("Are you sure you want to discard your changes?");
        alert.setContentText("You are about to revert all unsaved changes to this item. This action cannot be undone.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK) {
                // ... user chose OK

                if (selectedLTL != null) { // Discard current LTL
                    // Load file
                    try {
                        LTLFile originalFile = LMLIO.readXML(currentFile.getPath());

                        // Find current LTL
                        for (LTLAgent ltlAgent : originalFile.getLtlAgents()) {
                            if (ltlAgent.getName().equals(currentLTL.getName())) {
                                for (LTLAgent ltl : currentFile.getLtlAgents()) {
                                    if (ltl.getName().equals(ltlAgent.getName())) {
                                        currentFile.getLtlAgents().remove(ltl);
                                        currentFile.getLtlAgents().add(ltlAgent);
                                        break;
                                    }
                                }
                                FilePathTreeItem newLTLNode = getLTLNodeAtPath(currentFile, ltlAgent);
                                selectedFileNode.getChildren().add(newLTLNode);
                                selectedFileNode.getChildren().remove(selectedLTLNode);

                                // Resort items by name
                                selectedFileNode.getChildren().sort(Comparator.comparing(TreeItem::getValue));

                                treeView.getSelectionModel().select(newLTLNode);
                                selectedLTLNode = newLTLNode;
                                selectedLTL = ltlAgent;
                                changeEditLTL();
                                break;
                            }
                        }
                    } catch (JDOMException e) {
                        showExceptionAlert("Load Error", "Invalid file format", e);
                    } catch (IOException e) {
                        showExceptionAlert("Load Error", "Error opening file", e);
                    }
                } else {  // Discard current file


                    // Replace current file in file list

                    ltlFiles.remove(currentFile);

                    // Replace file node
                    FilePathTreeItem newFileNode = getFileNodeAtPath(currentFile.getPathAsPath());
                    selectedDirNode.getChildren().add(newFileNode);
                    selectedDirNode.getChildren().remove(selectedFileNode);

                    // Resort items by name
                    selectedDirNode.getChildren().sort(Comparator.comparing(TreeItem::getValue));

                    treeView.getSelectionModel().select(newFileNode);
                    selectedFileNode = newFileNode;
                    selectedFile = newFileNode.getLTLFile();
                    changeEditLTL();


            /*} catch (JDOMException e) {
                showExceptionAlert("Load Error", "Invalid file format", e);
            } catch (IOException e) {
                showExceptionAlert("Load Error", "Error opening file", e);
            }*/
                }

            }
        }


    }

    @Override
    public void onChanged() {
        setCurrentFileHasBeenModified(true);
        saved = false;
    }

    @FXML
    public void javaMopButtonClick() {
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("JavaMop Test Run Dialog");
        dialog.setHeaderText("Select input and output for JavaMop");

        // Set the button types.
        ButtonType closeButtonType = new ButtonType("Close", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(closeButtonType);

        // Create the input and output labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField input = new TextField();

        input.setDisable(true);
        TextField output = new TextField();

        output.setDisable(true);

        inputFile = null;
        outputFile = null;

        // Create browse button for input
        Button browseInput = new Button("Browse");
        browseInput.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();


            fileChooser.setTitle("Select File");

            fileChooser.setInitialDirectory(rootPath.toFile());


            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("MOP Files", "*.mop");
            fileChooser.getExtensionFilters().add(extFilter);




            //Show open file dialog
            File file = fileChooser.showOpenDialog(null);

            if(file!=null) {
                inputFile = file;
                input.setText(file.getPath());
            }
        });

        // Create browse button for output
        Button browseOutput = new Button("Browse");
        browseOutput.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();


            fileChooser.setTitle("Select File");

            fileChooser.setInitialDirectory(rootPath.toFile());

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JAVA Files", "*.java");
            fileChooser.getExtensionFilters().add(extFilter);




            //Show open file dialog
            File file = fileChooser.showOpenDialog(null);

            if(file!=null) {
                outputFile = file;
                output.setText(file.getPath());
            }
        });

        // Create button to run JavaMop
        Button mopButton = new Button("Run");

        // Create label for console output
        Label console = new Label("");
        ScrollPane sp = new ScrollPane();
        sp.setContent(console);
        sp.setMinWidth(400);
        sp.setMinHeight(200);

        grid.add(new Label("MOP:"), 0, 0);
        grid.add(input, 1, 0);
        grid.add(browseInput, 2, 0);
        grid.add(new Label("Target:"), 0, 1);
        grid.add(output, 1, 1);
        grid.add(browseOutput, 2, 1);
        grid.add(mopButton, 2, 2);
        grid.add(sp, 0, 3, 3, 3);



        // Enable/Disable run button depending on whether something is entered
        mopButton.setDisable(true);

        // Do some validation
        input.textProperty().addListener((observable, oldValue, newValue) -> mopButton.setDisable(newValue.trim().isEmpty()));
        output.textProperty().addListener((observable, oldValue, newValue) -> mopButton.setDisable(newValue.trim().isEmpty()));

        dialog.getDialogPane().setContent(grid);

        // Request focus on the input field by default.
        Platform.runLater(input::requestFocus);

        // For running
        mopButton.setOnAction(e -> {
            try {

                // Get current directory
                File workingDirectory = new File(rootPath.toString());

                String messageFromRunningJava = runJavaMopToolChain(inputFile, outputFile, workingDirectory, workingDirectory);



                console.setText(messageFromRunningJava);


            } catch (JavaMopRunningException e1) {
                showJavaMopExceptionAlert("JavaMop run error", "Problem running JavaMop", e1);
            } catch (IOException e1) {
                showExceptionAlert("JavaMop run error", "Problem running JavaMop", e1);
            }
        });

        dialog.setResultConverter(dialogButton -> null);

        @SuppressWarnings("unused") Optional result = dialog.showAndWait();
    }


    public void runJavaMop() {
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Run JavaMop | JavaMOP LTL Helper");
        dialog.setHeaderText("Select entry class for the project");

        // Set the button types.
        ButtonType closeButtonType = new ButtonType("Close", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(closeButtonType);

        // Create the input and output labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));


        TextField output = new TextField();
        output.setDisable(true);

        inputFile = null;
        outputFile = null;


        // Create browse button for output
        Button browseOutput = new Button("Browse");
        browseOutput.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();


            fileChooser.setTitle("Select File");

            fileChooser.setInitialDirectory(rootPath.toFile());

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JAVA Files", "*.java");
            fileChooser.getExtensionFilters().add(extFilter);




            //Show open file dialog
            File file = fileChooser.showOpenDialog(null);

            if(file!=null) {
                outputFile = file;
                output.setText(file.getPath());
            }
        });

        // Create button to run JavaMop
        Button mopButton = new Button("Run");

        // Create label for console output
        Label console = new Label("");
        ScrollPane sp = new ScrollPane();
        sp.setContent(console);
        sp.setMinWidth(400);
        sp.setMinHeight(200);

        grid.add(new Label("Target:"), 0, 1);
        grid.add(output, 1, 1);
        grid.add(browseOutput, 2, 1);
        grid.add(mopButton, 2, 2);
        grid.add(sp, 0, 3, 3, 3);



        // Enable/Disable run button depending on whether something is entered
        mopButton.setDisable(true);


        output.textProperty().addListener((observable, oldValue, newValue) -> mopButton.setDisable(newValue.trim().isEmpty()));

        dialog.getDialogPane().setContent(grid);



        // For running
        mopButton.setOnAction(e -> {
            try {
                console.setText("..."); // Show is processing

                // Get current directory
                File workingDirectory = new File(rootPath.toString());

                ArrayList<File> mopFiles = new ArrayList<>();   // List of mop files to use

                // Get package for the LTL
                String mopPackage = getMopPackage(selectedFileNode);

                // Get relative path to directory LML file is in
                Path pathToLMLDir = new File(selectedFile.getPath()).getParentFile().toPath();

                // Create directory for temp files
                File dirMop = new File(pathToLMLDir.toString());
                if (!dirMop.exists()) {
                    boolean created = dirMop.mkdir();
                    if (!created) {
                        throw new IOException("Unable to access target directory");
                    }
                }
                FileUtilities.cleanDirectory(workingDirectory);

                if (selectedLTL != null) { //Single LTL is selected
                    // Create mop file
                    LTLAgent currentLTL = selectedLTL;

                    // Set package for the LTL
                    currentLTL.setMopPackage(mopPackage);

                    File mopFile = new File(dirMop.getPath() + File.separator + selectedLTL.getName() + ".mop");
                    String content = currentLTL.toLongString(selectedFile.getGlobalEvents());
                    // if file doesn't exists, then create it
                    if (!mopFile.exists()) {
                        boolean fileCreated = mopFile.createNewFile();
                        if (!fileCreated)
                            throw new IOException("MOP file creation failed");
                    }
                    FileWriter fw = new FileWriter(mopFile.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(content);
                    bw.close();

                    mopFiles.add(mopFile);
                } else if (selectedFile != null) { // LML file is selected
                    for (LTLAgent currentLTL : selectedFile.getLtlAgents()) {

                        // Set package for the LTL
                        currentLTL.setMopPackage(mopPackage);

                        // Create mop file
                        File mopFile = new File(dirMop.getPath() + File.separator + currentLTL.getName() + ".mop");
                        String content = currentLTL.toLongString(selectedFile.getGlobalEvents());
                        // if file doesn't exists, then create it
                        if (!mopFile.exists()) {
                            boolean fileCreated = mopFile.createNewFile();
                            if (!fileCreated)
                                throw new IOException("MOP file creation failed");
                        }
                        FileWriter fw = new FileWriter(mopFile.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(content);
                        bw.close();

                        mopFiles.add(mopFile);
                    }
                }


                String messageFromRunningJava = runJavaMopToolChain(mopFiles, outputFile, workingDirectory, dirMop);


                console.setText(messageFromRunningJava);

                FileUtilities.cleanDirectory(dirMop);

            } catch (JavaMopRunningException e1) {
                showJavaMopExceptionAlert("JavaMop run error", "Problem running JavaMop", e1);
                console.setText("");
            } catch (IOException e1) {
                showExceptionAlert("JavaMop run error", "Problem running JavaMop", e1);
                console.setText("");
            }
        });

        dialog.setResultConverter(dialogButton -> null);

        @SuppressWarnings("unused") Optional result = dialog.showAndWait();

    }

    // Handle the running of javamop, rvm, ajc, and finally java, and return the results of the Java run
    private String runJavaMopToolChain(File mopFile, File targetJavaFile, File workingDirectory, File dirMop) throws JavaMopRunningException, IOException {


        ArrayList<File> mopFiles = new ArrayList<>();
        mopFiles.add(mopFile);

        return runJavaMopToolChain(mopFiles, targetJavaFile, workingDirectory, dirMop);
    }


    // Handle the running of javamop, rvm, ajc, and finally java, and return the results of the Java run
    private String runJavaMopToolChain(ArrayList<File> mopFiles, File targetJavaFile, File workingDirectory, File dirMop) throws JavaMopRunningException, IOException {
        String messageFromRunningJava;


        // Get name of target
        String className = targetJavaFile.getName();
        int lastPeriodPos = className.lastIndexOf('.');
        if (lastPeriodPos > 0)
            className = className.substring(0, lastPeriodPos);


        for (File mopFile : mopFiles) {
            // Get name of mop
            String mopName = mopFile.getName();
            lastPeriodPos = mopName.lastIndexOf('.');
            if (lastPeriodPos > 0)
                mopName = mopName.substring(0, lastPeriodPos);


            // Get name of output aspect and agent
            String aspectName =  dirMop.toString() + File.separator + mopName + "MonitorAspect.aj";
            String monitorName = dirMop.toString() + File.separator + mopName + ".rvm";

            // Run JavaMop
            String messageFromJavaMop = JavamopWrapper.runJMop(mopFile, dirMop, workingDirectory);
            // Check if output files were generated
            File aspectFile = new File(aspectName);
            File monitorFile = new File(monitorName);
            if (!aspectFile.exists() || !monitorFile.exists()) {
                throw new JavaMopRunningException("JavaMop running failed", messageFromJavaMop);
            }
            // Separate out BaseAspect
            JavamopWrapper.extractBaseAspect(aspectFile);

            // Run RV-Monitor
            String monitorLibName = dirMop.toString() + File.separator + mopName + "RuntimeMonitor.java";

            // Run
            String rvmOutput = RvmWrapper.runRvm(new File(monitorName), workingDirectory);

            // Check if output files were generated
            File monitorLibFile = new File(monitorLibName);
            if (!monitorLibFile.exists()) {
                throw new JavaMopRunningException("Rv-Monitor running failed", rvmOutput);
            }
        }


        // Run aspectj compiler
        String ajcOutput = AjcWrapper.runAjc(workingDirectory);
        // Check if output files were generated
        for (File mopFile : mopFiles) {
            // Get name of mop
            String mopName = mopFile.getName();
            lastPeriodPos = mopName.lastIndexOf('.');
            if (lastPeriodPos > 0)
                mopName = mopName.substring(0, lastPeriodPos);

            // Get name of output class
            String ajcClassName = dirMop.toString() + File.separator + mopName + "MonitorAspect.class";

            File ajcClassFile = new File(ajcClassName);
            if (!ajcClassFile.exists()) {
                throw new JavaMopRunningException("AJC running failed", ajcOutput);
            }
        }
        File targetClassFile = new File(targetJavaFile.getParent() + File.separator + className + ".class");
        if (!targetClassFile.exists()) {
            throw new JavaMopRunningException("AJC running failed", ajcOutput);
        }

        // Run the program
        messageFromRunningJava = JavaWrapper.runJava(targetJavaFile, workingDirectory);


        return messageFromRunningJava;
    }


    // Modifier selector box has been set
    public void modifierSelected() {
        if (selectedLTL != null) {
            String modifier = modifierSelectorBox.getValue();
            if (modifier.equals("standard"))
                modifier = "";

            if (!selectedLTL.getModifier().equals(modifier)) {
                selectedLTL.setModifier(modifier);
                setCurrentFileHasBeenModified(true);
            }


        }
    }

    public void addVariableClicked() {
        if (selectedFile != null && selectedLTL != null) {

            LTLAgent currentAgent = selectedLTL;
            AInstanceVar instanceVar = new AInstanceVar();
            String result = openVariableEditor(instanceVar);

            if (result.equals("Save")) {
                currentAgent.instanceVars.add(instanceVar);
                addAVariableButton(instanceVar);
                RegenerateLTLPane();
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        }
    }

    private void addAVariableButton(AInstanceVar instanceVar) {
        InstanceVarButton instanceVarButton = new InstanceVarButton(instanceVar);
        instanceVarButton.getVariableButton().setOnAction(e -> {
            String result = openVariableEditor(instanceVar);
            if (result.equals("Save")) {
                instanceVarButton.getVariableButton().setText(instanceVar.getType() + " " + instanceVar.getId());
                setCurrentFileHasBeenModified(true);
                saved = false;
                RegenerateLTLPane();
            } else if (result.equals("Remove")) {

                LTLAgent currentAgent = selectedLTL;
                currentAgent.instanceVars.remove(instanceVar);
                variableListDisplay.getChildren().remove(instanceVarButton);
                setCurrentFileHasBeenModified(true);
                saved = false;
                RegenerateLTLPane();
            }
        });
        variableListDisplay.getChildren().add(instanceVarButton);
    }

    private String openVariableEditor(AInstanceVar instanceVar) {
        return InstanceVarEditor.display(instanceVar);
    }


    // Exception to be thrown in a run step in the JavaMop tool chain fails to generated a required file
    private class JavaMopRunningException extends Exception {
        private String outputMessage;   // The output from the attempted run

        JavaMopRunningException(String errorMessage, String outputMessage) {
            super(errorMessage);

            this.outputMessage = outputMessage;
        }

        String getOutputMessage() {
            return this.outputMessage;
        }
    }

    @FXML
    public void addParameterClicked() {
        if (selectedFile != null && selectedLTL != null) {

            LTLAgent currentAgent = selectedLTL;
            Parameter parameter = new Parameter();
            String result = openParameterEditor(parameter);

            if (result.equals("Save")) {
                currentAgent.parameters.add(parameter);
                addParameterButton(parameter);
                RegenerateLTLPane();
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        }
    }



    private String openParameterEditor(Parameter parameter) {
        return ParameterEditor.display(parameter);
    }

    private void addParameterButton(Parameter parameter) {
        ParameterButton parameterButton = new ParameterButton(parameter);
        parameterButton.getParameterButton().setOnAction(e -> {
            String result = openParameterEditor(parameter);
            if (result.equals("Save")) {
                parameterButton.getParameterButton().setText(parameter.toLongString());
                setCurrentFileHasBeenModified(true);
                saved = false;
                RegenerateLTLPane();
            } else if (result.equals("Remove")) {

                LTLAgent currentAgent = selectedLTL;
                currentAgent.parameters.remove(parameter);
                parameterListDisplay.getChildren().remove(parameterButton);
                setCurrentFileHasBeenModified(true);
                saved = false;
                RegenerateLTLPane();
            }
        });
        parameterListDisplay.getChildren().add(parameterButton);
    }

    public void setEventMappingButtonClick() {
        Event event;

        event = eventSelectorBox.getValue();

        if (event != null) {
            String result = openEventMappingEditor(event);
            if (result.equals("Save")) {

                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        }
    }

    // Open event mapping editor for event
    private String openEventMappingEditor(Event event) {
        ArrayList<Parameter> allParameters = new ArrayList<>();

        if (selectedLTL != null)
            allParameters.addAll(selectedLTL.parameters);

        return EventMappingEditor.display(event, allParameters);
    }

    public void handlerConditionSelected() {
        if (selectedLTL != null) {
            if (passFailSelectorBox.getValue().equals("@violation")) {
                if (selectedLTL.getMatchFailCondition()) {
                    selectedLTL.setMatchFailCondition(false);
                    setCurrentFileHasBeenModified(true);
                }
            } else {
                if (!selectedLTL.getMatchFailCondition()) {
                    selectedLTL.setMatchFailCondition(true);
                    setCurrentFileHasBeenModified(true);
                }
            }


        }
    }


    // Shows a dialog window with the contents of current mop file
    public void viewMopFileWindow() {
        try {
            // Check if current file is savable
            Event nullEvent = selectedFile.eventHasNullExp();

            if (nullEvent != null) {
                throw new IOException("Global event " + nullEvent.getName() + " in file " + selectedFile.getPath() + " has undefined mapping.");
            }

            for (LTLAgent ltlAgent : selectedFile.getLtlAgents()) {
                nullEvent = ltlAgent.eventHasNullExp();

                if (nullEvent != null) {
                    throw new IOException("Event " + nullEvent.getName() + " in LTL " + ltlAgent.getName() + " in file " + selectedFile.getPath() + " has undefined mapping.");
                }

                if (ltlAgent.ltl.hasNullExp()) {
                    throw new IOException("LTL " + ltlAgent.getName() + " in file " + selectedFile.getPath() + " contains null expressions.");
                }
            }


            // Get package for the LTL
            String mopPackage = getMopPackage(selectedFileNode);
            selectedLTL.setMopPackage(mopPackage);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("MOP File Contents");
            alert.setHeaderText(selectedLTL.getName() + ".mop");
            TextArea textArea = new TextArea(selectedLTL.toLongString(selectedFile.getGlobalEvents()));
            textArea.setEditable(false);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            Button saveButton = new Button("Save");
            saveButton.setOnAction(e -> generateMopFile());

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(textArea, 0, 0);
            expContent.add(saveButton, 0, 1);

            // Set mop file text into the dialog pane.
            alert.getDialogPane().setExpandableContent(expContent);
            alert.getDialogPane().setExpanded(true);

            alert.showAndWait();
        } catch (IOException e) {
            showExceptionAlert("MOP Generation Error", "Unable to generate MOP file for this LTL", e);
        }
    }

    private final class TreeCellImpl extends TreeCell<String> {

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(getItem() == null ? "" : getItem());
                setGraphic(getTreeItem().getGraphic());

                // Add context menu
                final ContextMenu contextDirMenu = new ContextMenu();
                final ContextMenu contextFileMenu = new ContextMenu();
                final ContextMenu contextLTLMenu = new ContextMenu();

                // Menu for folders
                MenuItem newMenu = new MenuItem("New File...");
                newMenu.setOnAction(e -> newMenuClick());
                MenuItem newFolderMenu = new MenuItem("New Folder...");
                newFolderMenu.setOnAction(Controller.this::newFolderMenuClick);
                MenuItem reloadMenu = new MenuItem("Reload");
                reloadMenu.setOnAction(e -> reloadMenuClick());
                contextDirMenu.getItems().addAll(newMenu, newFolderMenu, reloadMenu);

                // Menu for files
                MenuItem addMenu = new MenuItem("New LTL...");
                addMenu.setOnAction(e -> addMenuClick());
                MenuItem renameFileMenu = new MenuItem("Rename File...");
                renameFileMenu.setOnAction(e -> renameFileMenuClick());
                MenuItem saveFileMenu = new MenuItem("Save");
                saveFileMenu.setOnAction(e -> saveCurrentMenuClick());
                MenuItem removeFileMenu = new MenuItem("Remove File");
                removeFileMenu.setOnAction(e -> removeFileMenuClick());
                contextFileMenu.getItems().addAll(addMenu, renameFileMenu, saveFileMenu, removeFileMenu);

                // Menu for LTLs
                addMenu = new MenuItem("New LTL...");
                addMenu.setOnAction(e -> addMenuClick());
                MenuItem renameLTLMenu = new MenuItem("Rename LTL");
                renameLTLMenu.setOnAction(e -> renameLTLMenuClick());
                MenuItem removeMenu = new MenuItem("Remove LTL");
                removeMenu.setOnAction(e -> removeMenuClick());
                MenuItem mopMenu = new MenuItem("Generate Mop...");
                mopMenu.setOnAction(e -> generateMopFile());
                contextLTLMenu.getItems().addAll(addMenu, renameLTLMenu, removeMenu, mopMenu);

                if (((FilePathTreeItem) getTreeItem()).isDirectory())
                    setContextMenu(contextDirMenu);
                else if (((FilePathTreeItem) getTreeItem()).isFile())
                    setContextMenu(contextFileMenu);
                else if (((FilePathTreeItem) getTreeItem()).isLTL())
                    setContextMenu(contextLTLMenu);
            }
        }
    }

    private void renameLTLMenuClick() {
        TextInputDialog dialog = new TextInputDialog(selectedLTL.getName());
        dialog.setTitle("Rename | JavaMOP LTL Helper");
        dialog.setHeaderText("Rename LTL");
        dialog.setContentText("What would you like to name the LTL?");

        Optional<String> result = dialog.showAndWait();

        // The Java 8 way to get the response value (with lambda expression).
        result.ifPresent(this::renameCurrentLTL);

        // Resort items by name
        selectedFileNode.getChildren().sort(Comparator.comparing(TreeItem::getValue));
    }

    private void renameCurrentLTL(String name) {
        try {
            for (TreeItem<String> ltlItem : selectedFileNode.getChildren()) {
                if (ltlItem.getValue().equals(name)) {
                    throw new Exception("LTL already exists in that file with that name");
                }
            }

            selectedLTL.setName(name);
            selectedLTLNode.setValue(name);

            setCurrentFileHasBeenModified(true);
            saved = false;
        }
        catch (Exception e) {
            showExceptionAlert("LTL Create Error", "Unable to rename LTL", e);
        }
    }

    private void removeFileMenuClick() {
        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
        confirm.initModality(Modality.WINDOW_MODAL);
        confirm.setTitle("Confirm Removal");
        confirm.setHeaderText("Confirm removal of file");
        confirm.setContentText("Are you sure you want to remove file " + selectedFileNode.getValue() + "?");

        Optional<ButtonType> result = confirm.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK) {
                try {
                    Files.delete(Paths.get(selectedFile.getPath()));
                    ltlFiles.remove(selectedFileNode.getLTLFile());
                    selectedDirNode.getChildren().remove(selectedFileNode);
                } catch (IOException e) {
                    showExceptionAlert("File I/O Error", "Unable to Remove File " + selectedFileNode.getValue(), e);
                }
            }
        }
    }

    private void renameFileMenuClick() {
        // Get file name to rename to
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Rename | JavaMOP LTL Helper");
        dialog.setHeaderText("Rename File");
        dialog.setContentText("What would you like to name your file?");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String filename = result.get();
            if (!filename.endsWith(".lml")) {
                filename = filename + ".lml";
            }
            String fullpath = selectedDirNode.getFullPath() + File.separator + filename;
            try {
                // File (or directory) with old name
                File oldFile = new File(selectedFile.getPath());

                // File (or directory) with new name
                File newFile = new File(fullpath);

                if (newFile.exists())
                    throw new java.io.IOException("file exists with that name");

                // Rename file (or directory)
                boolean success = oldFile.renameTo(newFile);

                if (!success) {
                    // File was not successfully renamed
                    throw new java.io.IOException("file could not be renamed");
                }

                // Rename file node and LTLFile
                selectedFile.setPath(fullpath);
                selectedFileNode.getLTLFile().setPath(fullpath);
                selectedFileNode.setFullPath(fullpath);
                // Set the node value
                if (!fullpath.endsWith(File.separator)) {
                    int indexOf = fullpath.lastIndexOf(File.separator);
                    if (indexOf > 0) {
                        selectedFileNode.setValue(fullpath.substring(indexOf + 1));
                    } else {
                        selectedFileNode.setValue(fullpath);
                    }
                }

                // Resort items by name
                selectedDirNode.getChildren().sort(Comparator.comparing(TreeItem::getValue));
            } catch (IOException e) {
                showExceptionAlert("File I/O Error", "Unable to Rename File " + filename, e);
            }
        }
    }

    private void reloadMenuClick() {
        FilePathTreeItem dirNode = selectedDirNode;


        // Confirm if their are unsaved changes before reload
        Boolean yesToAllSelected = false;

        for (TreeItem<String> fileNode : dirNode.getChildren()) {
            if (((FilePathTreeItem) fileNode).isFile()) {
                LTLFile ltlFile = ((FilePathTreeItem) fileNode).getLTLFile();

                if (ltlFile.getModified()) {
                    if (!yesToAllSelected) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.initModality(Modality.WINDOW_MODAL);
                        alert.setTitle("Confirm Reload");
                        alert.setHeaderText("You Have Unsaved Changes in file " + ltlFile.getPath());
                        alert.setContentText("Would you like to save your changes?");

                        ButtonType buttonTypeYesToAll = new ButtonType("Yes to All");
                        ButtonType buttonTypeYes = new ButtonType("Yes");
                        ButtonType buttonTypeNoToAll = new ButtonType("No to All");
                        ButtonType buttonTypeNo = new ButtonType("No");
                        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.OK_DONE);

                        alert.getButtonTypes().setAll(buttonTypeYesToAll, buttonTypeYes, buttonTypeNoToAll, buttonTypeNo, buttonTypeCancel);

                        Optional<ButtonType> result = alert.showAndWait();

                        if (result.isPresent()) {
                            if (result.get() == buttonTypeYes) {
                                // ... user chose "Yes"
                                try {
                                    LMLIO.writeXML(ltlFile, ltlFile.getPath());
                                    ltlFile.setModified(false);
                                } catch (IOException e) {
                                    showExceptionAlert("File I/O Error", "Unable to Save to File " + ltlFile.getPath(), e);
                                    return;
                                } catch (JDOMException e) {
                                    showExceptionAlert("Save Error", "Invalid format", e);
                                    return;
                                }
                            } else if (result.get() == buttonTypeYesToAll) {
                                // ... user chose "Yes to All"
                                yesToAllSelected = true;

                                try {
                                    LMLIO.writeXML(ltlFile, ltlFile.getPath());
                                    ltlFile.setModified(false);
                                } catch (IOException e) {
                                    showExceptionAlert("File I/O Error", "Unable to Save to File " + ltlFile.getPath(), e);
                                    return;
                                } catch (JDOMException e) {
                                    showExceptionAlert("Save Error", "Invalid format", e);
                                    return;
                                }
                            } else //noinspection StatementWithEmptyBody
                                if (result.get() == buttonTypeNo) {
                                // ... user chose "No"
                                } else if (result.get() == buttonTypeNoToAll) {
                                // ... user chose "No to All"
                                break;
                            } else {
                                // ... user chose CANCEL or closed the dialog
                                return;
                            }
                        }
                    } else {
                        try {
                            LMLIO.writeXML(ltlFile, ltlFile.getPath());
                            ltlFile.setModified(false);
                        } catch (IOException e) {
                            showExceptionAlert("File I/O Error", "Unable to Save to File " + ltlFile.getPath(), e);
                            return;
                        } catch (JDOMException e) {
                            showExceptionAlert("Save Error", "Invalid format", e);
                            return;
                        }
                    }
                }
            }
        }



        dirNode.getChildren().clear();
        String fullpath = dirNode.getFullPath();
        Path location = Paths.get(fullpath);

        try {
            DirectoryStream<Path> dir = Files.newDirectoryStream(location);
            for (Path file:dir) {
                BasicFileAttributes attributes = Files.readAttributes(file, BasicFileAttributes.class);
                if (attributes.isDirectory()) {
                    FilePathTreeItem childNode = getDirNodeAtPath(file);
                    dirNode.getChildren().add(childNode);
                } else {
                    if (file.toString().endsWith(".lml")) {
                        FilePathTreeItem childNode = getFileNodeAtPath(file);
                        dirNode.getChildren().add(childNode);
                    }
                }
            }

        }
        catch (IOException ex) {
            showExceptionAlert("Load Error", "Problem reading folder at location", ex);
        }
    }


    // Generate a file system directory node for a given path
    private FilePathTreeItem getDirNodeAtPath(Path location) {
        FilePathTreeItem directoryNode; // Node to represent directory
        String fullPath = location.toString();

        directoryNode = new FilePathTreeItem(fullPath);

        // Set the node value
        if (!fullPath.endsWith(File.separator)) {
            int indexOf = fullPath.lastIndexOf(File.separator);
            if (indexOf > 0) {
                directoryNode.setValue(fullPath.substring(indexOf + 1));
            } else {
                directoryNode.setValue(fullPath);
            }
        }

        // Set the node image
        directoryNode.setGraphic(new ImageView(new Image(
                getClass().getResourceAsStream("imgs/folder.png"))));

        // Set directory expanded event

        try {
            DirectoryStream<Path> dir = Files.newDirectoryStream(location);
            for (Path file:dir) {
                BasicFileAttributes attributes = Files.readAttributes(file, BasicFileAttributes.class);
                if (attributes.isDirectory()) {
                    FilePathTreeItem childNode = getDirNodeAtPath(file);
                    directoryNode.getChildren().add(childNode);
                } else {
                    if (file.toString().endsWith(".lml")) {
                        FilePathTreeItem childNode = getFileNodeAtPath(file);
                        directoryNode.getChildren().add(childNode);
                    }
                }

                // Resort items by name
                directoryNode.getChildren().sort(Comparator.comparing(TreeItem::getValue));
            }

        }
        catch (IOException ex) {
            showExceptionAlert("Load Error", "Problem loading folder at location", ex);
        }


        directoryNode.setFullPath(fullPath);

        directoryNode.setDirectory();

        return directoryNode;
    }

    private FilePathTreeItem getFileNodeAtPath(Path file) {
        FilePathTreeItem fileNode;  // Node to represent file
        LTLFile ltlFile;               // Representation of file itself
        String fullPath = file.toString();

        fileNode = new FilePathTreeItem(fullPath);

        // Set the node value
        if (!fullPath.endsWith(File.separator)) {
            int indexOf = fullPath.lastIndexOf(File.separator);
            if (indexOf > 0) {
                fileNode.setValue(fullPath.substring(indexOf + 1));
            } else {
                fileNode.setValue(fullPath);
            }
        }

        // Set the node image
        fileNode.setGraphic(new ImageView(new Image(
                getClass().getResourceAsStream("imgs/new-document.png"))));


        fileNode.setFile();
        fileNode.setFullPath(fullPath);

        // Load file
        try {
            ltlFile = LMLIO.readXML(fullPath);
            ltlFile.setPathAsPath(file);
            fileNode.setLTLFile(ltlFile);

            // Add all LTLs
            for (LTLAgent ltlAgent : ltlFile.getLtlAgents()) {
                FilePathTreeItem ltlNode = getLTLNodeAtPath(ltlFile, ltlAgent);
                fileNode.getChildren().add(ltlNode);
            }

            // Resort items by name
            fileNode.getChildren().sort(Comparator.comparing(TreeItem::getValue));
        } catch (JDOMException e) {
            showExceptionAlert("Load Error", "Invalid file format", e);
            fileNode.setValue(fileNode.getValue() + "(!)");
        } catch (IOException e) {
            showExceptionAlert("Load Error", "Error opening file", e);
            fileNode.setValue(fileNode.getValue() + "(!)");
        }

        return fileNode;
    }

    // Gets the string representation of the proper package for LTLs in a file based on its directory path
    private String getMopPackage(FilePathTreeItem fileNode) {
        FilePathTreeItem parentDirectory = (FilePathTreeItem) fileNode.getParent();

        if (parentDirectory == treeView.getRoot()) {
            return "";
        } else {
            String parentPackage = getMopPackage(parentDirectory);
            if (parentPackage.equals(""))
                return parentDirectory.getValue();
            else
                return parentPackage + "." + parentDirectory.getValue();
        }
    }

    private FilePathTreeItem getLTLNodeAtPath(LTLFile ltlFile, LTLAgent ltlAgent) {
        FilePathTreeItem ltlNode;       // Node to represent LTL
        ltlNode = new FilePathTreeItem(ltlAgent.getName());

        // Set the node image
        ltlNode.setGraphic(new ImageView(new Image(
                getClass().getResourceAsStream("imgs/Black_Board.png"))));


        ltlNode.setLTL();
        ltlNode.setLTL(ltlAgent);
        ltlNode.setLTLFile(ltlFile);

        return ltlNode;
    }

    // Set that current file has been modified
    private void setCurrentFileHasBeenModified(Boolean modified) {
        selectedFile.setModified(modified);

        String fullPath = selectedFileNode.getFullPath();

        if (modified) {
            // Set the node value
            if (!fullPath.endsWith(File.separator)) {
                int indexOf = fullPath.lastIndexOf(File.separator);
                if (indexOf > 0) {
                    selectedFileNode.setValue(fullPath.substring(indexOf + 1) + " *");
                } else {
                    selectedFileNode.setValue(fullPath + " *");
                }
            }
        }
        else {
            // Set the node value
            if (!fullPath.endsWith(File.separator)) {
                int indexOf = fullPath.lastIndexOf(File.separator);
                if (indexOf > 0) {
                    selectedFileNode.setValue(fullPath.substring(indexOf + 1));
                } else {
                    selectedFileNode.setValue(fullPath);
                }
            }
        }

        // Set up so that close will check if file has been modified
        Stage stage = (Stage) addEventButton.getScene().getWindow();
        stage.setOnCloseRequest(eh -> {
            eh.consume();
            exitMenuClick();
        });
    }

    private void descriptionChanged()
    {
        // int selectedIndex = listView.getSelectionModel().getSelectedIndex();
        String newDescription = descriptionText.getText();
        FilePathTreeItem selectedItem = (FilePathTreeItem) treeView.getSelectionModel().getSelectedItem();

        if (selectedItem.isLTL())
            if (!selectedLTL.getDescription().equals(newDescription)) {
                selectedLTL.setDescription(newDescription);
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        else if (selectedItem.isFile())
            if (!selectedFile.getDescription().equals(newDescription)) {
                selectedFile.setDescription(newDescription);
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
    }

    private void propertyHandlerChanged()
    {
        String newHandler = propertyHandlerText.getText();
        FilePathTreeItem selectedItem = (FilePathTreeItem) treeView.getSelectionModel().getSelectedItem();

        if (selectedItem.isLTL())
            if (!selectedLTL.getPropertyHandler().equals(newHandler)) {
                selectedLTL.setPropertyHandler(newHandler);
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
    }

    private void changeEditLTL() {

        if (selectedLTL != null && selectedFile != null)
        {
            parameterPane.setVisible(true);
            variablePane.setVisible(true);
            eventMappingPane.setVisible(true);
            localEventPane.setVisible(true);
            ltlCreationPane.setVisible(true);
            propertyHandlerPane.setVisible(true);
            descriptionText.setVisible(true);
            modifierSelectorBox.setVisible(true);

            LTLAgent currentAgent = selectedLTL;
            descriptionText.setText(currentAgent.getDescription());


            parameterListDisplay.getChildren().clear();
            variableListDisplay.getChildren().clear();


            // Create buttons for parameters in LTL
            currentAgent.parameters.forEach(this::addParameterButton);

            // Create buttons for instance variables in LTL
            currentAgent.instanceVars.forEach(this::addAVariableButton);

            // Create uneditable buttons for global events in file
            eventsListDisplay.getChildren().clear();
            eventSelectorBox.getItems().clear();
            for (Event event : selectedFile.getGlobalEvents()) {
                EventButton eventButton = new EventButton(event);
                eventButton.setDisable(true);
                eventsListDisplay.getChildren().add(eventButton);
            }

            // Create buttons for events in LTL
            currentAgent.events.forEach(this::addEventButton);

            // Fill property handler
            propertyHandlerPackagesField.setText(PackageListHandler.getPackagesString(currentAgent.getPropertyPackages()));
            propertyHandlerText.setText(currentAgent.getPropertyHandler());
            if (currentAgent.getMatchFailCondition())
                passFailSelectorBox.setValue("@validation");
            else
                passFailSelectorBox.setValue("@violation");

            // Fill modifier
            if (currentAgent.getModifier().equals(""))
                modifierSelectorBox.setValue("standard");
            else
                modifierSelectorBox.setValue(currentAgent.getModifier());

            // Create panel for LTL display
            if (currentAgent.ltl.getType().equals("Helper"))
                tabPane.getSelectionModel().select(helperTab);
            else
                tabPane.getSelectionModel().select(customTab);

            ArrayList<Event> allEvents = new ArrayList<>();
            allEvents.addAll(selectedFile.getGlobalEvents());
            allEvents.addAll(currentAgent.events);
            switch (currentAgent.ltl.getType())
            {
                case "Helper":
                    ltlDisplay.setContent(new HelperLTLContainer((HelperLTL) currentAgent.ltl, allEvents, new AddEventAction(), this));
                    ltlDisplay.setFitToWidth(false);
                    break;
                case "Custom":
                    CustomLTL customLTL = (CustomLTL) currentAgent.ltl;
                    TextField customLTLField = new TextField(customLTL.getContent());
                    ltlDisplay.setContent(customLTLField);
                    ltlDisplay.setFitToWidth(true);
                    customLTLField.textProperty().addListener((observable, oldValue, newValue) -> {
                        customLTL.setContent(customLTLField.getText());
                        setCurrentFileHasBeenModified(true);
                        saved = false;
                    });
                    break;
            }
        } else if (selectedFile != null) {
            parameterPane.setVisible(false);
            variablePane.setVisible(false);
            eventMappingPane.setVisible(true);
            localEventPane.setVisible(true);
            ltlCreationPane.setVisible(false);
            propertyHandlerPane.setVisible(false);
            descriptionText.setVisible(true);
            modifierSelectorBox.setVisible(false);

            LTLFile currentFile = selectedFile;

            descriptionText.setText(currentFile.getDescription());



            // Create buttons for global events
            eventsListDisplay.getChildren().clear();
            eventSelectorBox.getItems().clear();
            currentFile.getGlobalEvents().forEach(this::addGlobalEventButton);
        }
        else {
            parameterPane.setVisible(false);
            variablePane.setVisible(false);
            eventMappingPane.setVisible(false);
            localEventPane.setVisible(false);
            ltlCreationPane.setVisible(false);
            propertyHandlerPane.setVisible(false);
            descriptionText.setVisible(false);
            modifierSelectorBox.setVisible(false);
        }

    }


    // Create event button for given event
    private void addEventButton(Event event)
    {
        EventButton eventButton = new EventButton(event);
        eventButton.getEventButton().setOnAction(e -> {

            String result = openEventEditor(event);
            if (result.equals("Save")) {
                eventButton.getEventButton().setText(event.getName());
                setCurrentFileHasBeenModified(true);
                saved = false;
                RegenerateLTLPane();
            } else if (result.equals("Remove")) {
                try {

                    LTLAgent currentAgent = selectedLTL;
                    if (currentAgent.ltl.hasEvent(event)) {
                        throw new Exception("Event in use in LTL");
                    }
                    currentAgent.events.remove(event);
                    eventsListDisplay.getChildren().remove(eventButton);
                    setCurrentFileHasBeenModified(true);
                    saved = false;
                    RegenerateLTLPane();
                } catch (Exception ex) {
                    showExceptionAlert("Remove Error", "Unable to Remove Event", ex);
                }
            }
        });
        eventsListDisplay.getChildren().add(eventButton);
        eventSelectorBox.getItems().add(event);
    }

    // If events changed, need to regenerate events lists in LTL pane
    private void RegenerateLTLPane()
    {
        LTLAgent currentAgent = selectedLTL;

        ArrayList<Event> allEvents = new ArrayList<>();
        allEvents.addAll(selectedFile.getGlobalEvents());
        allEvents.addAll(currentAgent.events);
        switch (currentAgent.ltl.getType())
        {
            case "Helper":
                ltlDisplay.setContent(new HelperLTLContainer((HelperLTL) currentAgent.ltl, allEvents, new AddEventAction(), this));
                ltlDisplay.setFitToWidth(false);
                break;
            case "Custom":
                CustomLTL customLTL = (CustomLTL) currentAgent.ltl;
                TextField customLTLField = new TextField(customLTL.getContent());
                ltlDisplay.setContent(customLTLField);
                ltlDisplay.setFitToWidth(true);
                customLTLField.textProperty().addListener((observable, oldValue, newValue) -> {
                    customLTL.setContent(customLTLField.getText());
                    setCurrentFileHasBeenModified(true);
                    saved = false;
                });
                break;
        }
    }

    // Open event editor for a given event
    private String openEventEditor(Event event)
    {
        return EventEditor.display(event);
    }


    @FXML
    public void newMenuClick()
    {

        // Get file name to load from
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("New | JavaMOP LTL Helper");
        dialog.setHeaderText("Create New File");
        dialog.setContentText("What would you like to name your file?");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String filename = result.get();
            if (!filename.endsWith(".lml")) {
                filename = filename + ".lml";
            }
            String fullpath = selectedDirNode.getFullPath() + File.separator + filename;
            try {
                for (TreeItem<String> fileItem : selectedDirNode.getChildren()) {
                    if (fileItem.getValue().equals(fullpath)) {
                        throw new IOException("File already exists at that location with that name");
                    }
                }

                // Create new empty file
                LTLFile newFile = new LTLFile();
                newFile.setPath(fullpath);
                LMLIO.writeXML(newFile, fullpath);
                FilePathTreeItem newFileNode = getFileNodeAtPath(Paths.get(fullpath));
                selectedDirNode.getChildren().add(newFileNode);
            } catch (IOException e) {
                showExceptionAlert("File I/O Error", "Unable to Create File " + filename, e);
            } catch (JDOMException e) {
                showExceptionAlert("Save Error", "Invalid format", e);
            }
        }
    }


    @FXML
    public void saveMenuClick() {

        String pathOfFileWithError = "";  // A file that caused exception to be thrown

        try {
            for (LTLFile ltlFile : ltlFiles) {
                pathOfFileWithError = ltlFile.getPath();

                // Check if current file is savable
                Event nullEvent = selectedFile.eventHasNullExp();

                if (nullEvent != null) {
                    throw new IOException("Global event " + nullEvent.getName() + " in file " + selectedFile.getPath() + " has undefined mapping.");
                }

                for (LTLAgent ltlAgent : selectedFile.getLtlAgents()) {
                    nullEvent = ltlAgent.eventHasNullExp();

                    if (nullEvent != null) {
                        throw new IOException("Event " + nullEvent.getName() + " in LTL " + ltlAgent.getName() + " in file " + selectedFile.getPath() + " has undefined mapping.");
                    }

                    if (ltlAgent.ltl.hasNullExp()) {
                        throw new IOException("LTL " + ltlAgent.getName() + " in file " + selectedFile.getPath() + " contains null expressions.");
                    }
                }

                LMLIO.writeXML(ltlFile, ltlFile.getPath());
                ltlFile.setModified(false);

            }

            InitializeTree(rootPath);
            saved = true;
        } catch (IOException e) {
            showExceptionAlert("File I/O Error", "Unable to Save to File " + pathOfFileWithError, e);
        } catch (JDOMException e) {
            showExceptionAlert("Save Error", "Invalid format", e);
        }
    }


    @FXML
    public void saveCurrentMenuClick() {
        try {
            // Check if current file is savable
            Event nullEvent = selectedFile.eventHasNullExp();

            if (nullEvent != null) {
                throw new IOException("Global event " + nullEvent.getName() + " in file " + selectedFile.getPath() + " has undefined mapping.");
            }

            for (LTLAgent ltlAgent : selectedFile.getLtlAgents()) {
                nullEvent = ltlAgent.eventHasNullExp();

                if (nullEvent != null) {
                    throw new IOException("Event " + nullEvent.getName() + " in LTL " + ltlAgent.getName() + " in file " + selectedFile.getPath() + " has undefined mapping.");
                }

                if (ltlAgent.ltl.hasNullExp()) {
                    throw new IOException("LTL " + ltlAgent.getName() + " in file " + selectedFile.getPath() + " contains null expressions.");
                }
            }

            LMLIO.writeXML(selectedFile, selectedFile.getPath());
            setCurrentFileHasBeenModified(false);
        } catch (IOException e) {
            showExceptionAlert("File I/O Error", "Unable to Save to File " + selectedFile.getPath(), e);
        } catch (JDOMException e) {
            showExceptionAlert("Save Error", "Invalid format", e);
        }
    }

    @FXML
    public void exitMenuClick()
    {
        Boolean yesToAllSelected = false;

        for (LTLFile ltlFile : ltlFiles) {
            if (ltlFile.getModified()) {
                if (!yesToAllSelected) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.initModality(Modality.WINDOW_MODAL);
                    alert.setTitle("Confirm Exit");
                    alert.setHeaderText("You Have Unsaved Changes in file " + ltlFile.getPath());
                    alert.setContentText("Would you like to save your changes?");

                    ButtonType buttonTypeYesToAll = new ButtonType("Yes to All");
                    ButtonType buttonTypeYes = new ButtonType("Yes");
                    ButtonType buttonTypeNoToAll = new ButtonType("No to All");
                    ButtonType buttonTypeNo = new ButtonType("No");
                    ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.OK_DONE);

                    alert.getButtonTypes().setAll(buttonTypeYesToAll, buttonTypeYes, buttonTypeNoToAll, buttonTypeNo, buttonTypeCancel);

                    Optional<ButtonType> result = alert.showAndWait();


                    if (result.isPresent()) {
                        if (result.get() == buttonTypeYes) {
                            // ... user chose "Yes"
                            try {
                                LMLIO.writeXML(ltlFile, ltlFile.getPath());
                                ltlFile.setModified(false);
                            } catch (IOException e) {
                                showExceptionAlert("File I/O Error", "Unable to Save to File " + ltlFile.getPath(), e);
                                return;
                            } catch (JDOMException e) {
                                showExceptionAlert("Save Error", "Invalid format", e);
                                return;
                            }
                        } else if (result.get() == buttonTypeYesToAll) {
                            // ... user chose "Yes to All"
                            yesToAllSelected = true;

                            try {
                                LMLIO.writeXML(ltlFile, ltlFile.getPath());
                                ltlFile.setModified(false);
                            } catch (IOException e) {
                                showExceptionAlert("File I/O Error", "Unable to Save to File " + ltlFile.getPath(), e);
                                return;
                            } catch (JDOMException e) {
                                showExceptionAlert("Save Error", "Invalid format", e);
                                return;
                            }
                        } else //noinspection StatementWithEmptyBody
                            if (result.get() == buttonTypeNo) {
                            // ... user chose "No"
                            } else if (result.get() == buttonTypeNoToAll) {
                            // ... user chose "No to All"
                            break;
                        } else {
                            // ... user chose CANCEL or closed the dialog
                            return;
                        }
                    }
                }
                else {
                    try {
                        LMLIO.writeXML(ltlFile, ltlFile.getPath());
                        ltlFile.setModified(false);
                    } catch (IOException e) {
                        showExceptionAlert("File I/O Error", "Unable to Save to File " + ltlFile.getPath(), e);
                        return;
                    } catch (JDOMException e) {
                        showExceptionAlert("Save Error", "Invalid format", e);
                        return;
                    }
                }
            }
        }

        Stage stage = (Stage) addEventButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void addMenuClick()
    {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("New | JavaMOP LTL Helper");
        dialog.setHeaderText("Add New LTL");
        dialog.setContentText("What would you like to name your LTL?");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String ltlName = result.get();
            try {
                for (TreeItem<String> ltlItem : selectedFileNode.getChildren()) {
                    if (ltlItem.getValue().equals(ltlName)) {
                        throw new Exception("LTL already exists in that file with that name");
                    }
                }

                LTLAgent newAgent = new LTLAgent();
                newAgent.setName(ltlName);
                selectedFile.getLtlAgents().add(newAgent);

                FilePathTreeItem ltlNode = getLTLNodeAtPath(selectedFile, newAgent);
                selectedFileNode.getChildren().add(ltlNode);

                setCurrentFileHasBeenModified(true);
                saved = false;
            }
            catch (Exception e) {
                showExceptionAlert("LTL Create Error", "Unable to create LTL", e);
            }

        }
    }

    @FXML
    public void removeMenuClick()
    {
        // Confirm action
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.setTitle("Confirm Remove");
        alert.setHeaderText("Are you sure you want to remove this LTL?");
        alert.setContentText("You are about to remove this LTL. This action cannot be undone.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK) {
                // ... user chose OK

                if (selectedLTLNode != null) {
                    FilePathTreeItem ltlToRemove = selectedLTLNode;
                    selectedLTLNode = null;
                    selectedLTL = null;
                    selectedFile.getLtlAgents().remove(ltlToRemove.getLtl());
                    selectedFileNode.getChildren().remove(ltlToRemove);

                    setCurrentFileHasBeenModified(true);
                    saved = false;

                    treeView.getSelectionModel().select(selectedFileNode);
                    removeLTLButton.setDisable(true);
                    removeMenu.setDisable(true);
                }
            }
        }
    }


    @FXML
    public void addEventClicked()
    {
        if (selectedFile != null && selectedLTL != null) {
            LTLAgent currentAgent = selectedLTL;
            Event event = new Event();
            String result = openEventEditor(event);

            if (result.equals("Save")) {
                currentAgent.events.add(event);
                addEventButton(event);
                RegenerateLTLPane();
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        } else if (selectedFile != null) {
            Event event = new Event();
            String result = openEventEditor(event);

            if (result.equals("Save"))
            {
                selectedFile.getGlobalEvents().add(event);
                addGlobalEventButton(event);

                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        }
    }

    @FXML
    public void tabClicked() {
        LTLAgent currentAgent = selectedLTL;

        ArrayList<Event> allEvents = new ArrayList<>();
        allEvents.addAll(selectedFile.getGlobalEvents());
        allEvents.addAll(currentAgent.events);
        switch (tabPane.getSelectionModel().getSelectedItem().getText())
        {
            case "Helper":
                try {
                    if (currentAgent.ltl.getType().equals("Custom")) {

                        // Parse current custom LTL to create helper LTL
                        Formula ltlFormula = Formula.parse(currentAgent.ltl.toLongString());


                        currentAgent.ltl = HelperCreator.getLTL(ltlFormula, allEvents);
                    }

                    ltlDisplay.setContent(new HelperLTLContainer((HelperLTL) currentAgent.ltl, allEvents, new AddEventAction(), this));
                    ltlDisplay.setFitToWidth(false);
                } catch (ParseErrorException e) {
                    showExceptionAlert("Parse Error", "Syntax Error in LTL", e);
                    tabPane.getSelectionModel().select(customTab);
                }
                break;
            case "Custom":
                currentAgent.ltl = new CustomLTL(currentAgent.ltl.toLongString());
                CustomLTL customLTL = (CustomLTL) currentAgent.ltl;
                TextField customLTLField = new TextField(customLTL.getContent());
                ltlDisplay.setContent(customLTLField);
                ltlDisplay.setFitToWidth(true);
                customLTLField.textProperty().addListener((observable, oldValue, newValue) -> customLTL.setContent(customLTLField.getText()));

                break;
        }
    }

    // Create event button for global event
    private void addGlobalEventButton(Event event) {
        EventButton eventButton = new EventButton(event);
        eventButton.getEventButton().setOnAction(e -> {
            String result = openEventEditor(event);
            if (result.equals("Save")) {
                eventButton.getEventButton().setText(event.getName());

                setCurrentFileHasBeenModified(true);
                saved = false;
            } else if (result.equals("Remove")) {
                try {
                    for (LTLAgent agent : selectedFile.getLtlAgents()) {
                        if (agent.ltl.hasEvent(event)) {
                            throw new Exception("Event in use in LTL " + agent.getName());
                        }
                    }

                    selectedFile.getGlobalEvents().remove(event);
                    eventsListDisplay.getChildren().remove(eventButton);
                    setCurrentFileHasBeenModified(true);
                    saved = false;
                } catch (Exception ex) {
                    showExceptionAlert("Remove Error", "Unable to Remove Event", ex);
                }
            }
        });
        if (selectedFile != null && selectedLTL != null)
            eventButton.setDisable(true);
        else
            eventSelectorBox.getItems().add(event);
        eventsListDisplay.getChildren().add(eventButton);
    }

    // Select root folder of project
    public void openMenuClicked() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select the root source folder for the project");
        directoryChooser.setInitialDirectory(new File(rootPath.toString()));
        File selectedDirectory =
                directoryChooser.showDialog(new Stage());

        if(selectedDirectory != null)
            InitializeTree(selectedDirectory.toPath());

        saved = true;
    }

    public void newFolderMenuClick(ActionEvent actionEvent) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("New Folder | JavaMOP LTL Helper");
        dialog.setHeaderText("Create New Subfolder");
        dialog.setContentText("What would you like to name your folder?");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String dirname = result.get();

            String fullPath = selectedDirNode.getFullPath() + File.separator + dirname;
            try {
                for (TreeItem<String> fileItem : selectedDirNode.getChildren()) {
                    if (fileItem.getValue().equals(fullPath)) {
                        throw new IOException("Folder already exists at that location with that name");
                    }
                }

                File dir = new File(fullPath);

                // attempt to create the directory here
                boolean successful = dir.mkdir();
                if (!successful) {
                    throw new IOException("Failed to create the folder");
                }


                FilePathTreeItem newDirNode = getDirNodeAtPath(Paths.get(fullPath));
                selectedDirNode.getChildren().add(newDirNode);
            } catch (IOException e) {
                showExceptionAlert("File I/O Error", "Unable to Create Folder " + dirname, e);
            }
        }
    }

    private class AddEventAction implements Command
    {

        @Override
        public void execute(Object data) {
            LTLAgent currentAgent = selectedLTL;
            Event event = new Event();
            String result = openEventEditor(event);

            if (result.equals("Save"))
            {
                currentAgent.events.add(event);
                addEventButton(event);
                callCommand((Command) data, event.getName());
                RegenerateLTLPane();
                setCurrentFileHasBeenModified(true);
                saved = false;
            }
        }
    }


    // Execute command passed in with passed data as parameter
    private static void callCommand(Command command, Object data)
    {
        command.execute(data);
    }


    // Shows an alert box for an exception
    private void showExceptionAlert(String title, String header, Exception e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(e.getMessage());

        // Create expandable Exception
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    // Shows an alert box for an exception from attempting to run JavaMop
    private void showJavaMopExceptionAlert(String title, String header, JavaMopRunningException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(e.getMessage());

        // Create expandable Exception
        String exceptionText = e.getOutputMessage();

        Label label = new Label("The running output was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

}
