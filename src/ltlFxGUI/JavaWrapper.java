package ltlFxGUI;

import java.io.*;

/**
 * Created by Rex Cope
 *
 * Wrapper for running java within the program
 */
class JavaWrapper {

    // Run java given entry point file and working directory
    static String runJava(File inputJavaFile, File workingDirectory) throws IOException {
        String messagesFromRunning = "";

        Process process;



        // Get relative path to main file
        String relativePathToJavaFile = workingDirectory.toPath().relativize(inputJavaFile.getParentFile().toPath()).toString();


        // Get name of entry class
        String className = inputJavaFile.getName();
        int lastPeriodPos = className.lastIndexOf('.');
        if (lastPeriodPos > 0)
            className = className.substring(0, lastPeriodPos);

        // Get path to entry point
        String entryPointString;
        if (workingDirectory.equals(inputJavaFile.getParentFile())) {
            entryPointString = className;
        } else {
            entryPointString = relativePathToJavaFile + "/" + className;
        }

        System.out.println("> java " + entryPointString + "\n");

        ProcessBuilder builder = new ProcessBuilder("java", entryPointString);
        builder.directory(workingDirectory);
        builder.redirectErrorStream(true);
        process = builder.start();


        InputStream inputStream = process.getInputStream();
        InputStream errorInputStream = process.getErrorStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        InputStreamReader errorsInputStreamReader = new InputStreamReader(errorInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        BufferedReader errorBufferedReader = new BufferedReader(errorsInputStreamReader);
        String line;


        while ((line = bufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        while ((line = errorBufferedReader.readLine()) != null) {
            messagesFromRunning += line + "\n";
        }
        messagesFromRunning += "\n";

        System.out.println(messagesFromRunning);

        return messagesFromRunning;
    }
}
