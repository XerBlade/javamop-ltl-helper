package ltlFxGUI;

import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import ltlAgentPkg.Parameter;

/**
 * Created by Rex Cope
 */
class ParameterButton extends StackPane {
    private Button parameterButton;     // Actual button to edit parameter
    private Parameter parameter;            // Parameter represented

    ParameterButton(Parameter parameter)
    {
        this.parameter = parameter;

        parameterButton = new Button(parameter.toLongString());

        this.getChildren().add(parameterButton);
    }

    Button getParameterButton()
    {
        return this.parameterButton;
    }

    public Parameter getParameter()
    {
        return this.parameter;
    }
}
