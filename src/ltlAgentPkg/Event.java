package ltlAgentPkg;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * @author Rex Cope
 *
 * Represent a single event/predicate
 *
 */


public class Event {
    private String name;    // Identifier of the event
    private String description;        // Description of event
    public ArrayList<Parameter> parameters = new ArrayList<>();
    private String when;         // false for before, true for after
    private Parameter afterParameter;   // Parameter used with after returning or throwing
    public Pointcut pointcut;     // definition of the event
    private String action;      // Code to execute on event, if any
    private ConcurrentSkipListSet<String> advicePackages = new ConcurrentSkipListSet<>();   // Packages needed by the Java code in the advice

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getWhen() {
        return this.when;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return this.action;
    }

    public ConcurrentSkipListSet<String> getAdvicePackages() {
        return advicePackages;
    }

    // Set type of expression
    public void setCutType(String type) {
        switch (type) {
            case "call":
            case "execution":
            case "get":
            case "set":
            case "handler":
            case "staticinitialization":
            case "initialization":
            case "preinitialization":
            case "condition":
                pointcut = new ExpCut(type);
                break;
            case "this":
            case "target":
            case "args":
                pointcut = new ParamCut(type);
                break;
            case "!":
                pointcut = new UnaryOpCut(type);
                break;
            case "&&":
            case "||":
                pointcut = new BinaryOpCut(type);
                break;
            case "(  )":
                pointcut = new ParenCut();
                break;
            default:
                pointcut = new ExpCut("call");
                break;
        }
    }

    public void setAfterParameter(Parameter parameter) {
        this.afterParameter = parameter;
    }

    public Parameter getAfterParameter() {
        return this.afterParameter;
    }

    public String toString() {
        return name;
    }

    public String toLongString() {
        String representation;


        representation = getHeaderRepresentation();
        representation += " {";
        representation += "\n\t\t/* " + description + " */\n";
        representation += "\t\t" + action + "\n\t}";

        return representation;
    }

    public String getHeaderRepresentation() {
        String representation;


        representation = "event ";
        representation += name + " ";
        switch (when) {
            case "after":
                representation += "after(";
                representation += getParameterListRepresentation() + ")";
                break;
            case "before":
                representation += "before(";
                representation += getParameterListRepresentation() + ")";
                break;
            case "after returning":
                representation += "after(";
                representation += getParameterListRepresentation() + ")";
                if (afterParameter != null)
                    representation += " returning (" + afterParameter.toLongString() + ")";
                break;
            case "after throwing":
                representation += "after(";
                representation += getParameterListRepresentation() + ")";
                if (afterParameter != null)
                    representation += " throwing (" + afterParameter.toLongString() + ")";
                break;
        }
        if (pointcut != null)
            representation += " : " + pointcut.toLongString();
        else
            representation += " : " + "null";

        return representation;
    }

    public String getParameterListRepresentation() {
        generateUsedParameters();

        String representation = "";
        int num = 0;       // Number of items that have been parsed from list

        for (Parameter parameter : parameters) {
            if (num > 0)
                representation += ", ";
            representation += parameter.toLongString();
            num++;
        }

        return representation;
    }

    // Default constructor
    public Event() {
        this.name = "event" + ((int) (Math.random() * (9999 - 1))) + 1;
        when = "before";
        action = "";
    }

    // Constructor initializes name
    public Event(String name) {
        this.name = name;
        when = "before";
        action = "";
    }

    public Boolean hasNullExp() {
        return (pointcut == null) || pointcut.hasNullExp();
    }

    // Keeps up the parameter list
    public void generateUsedParameters() {
        if (pointcut != null) {
            parameters = pointcut.getAllUsedParameters();
        }
    }

    // Get all the packages needed in the event
    ConcurrentSkipListSet<String> getAllPackages() {
        ConcurrentSkipListSet<String> allPackages = new ConcurrentSkipListSet<>(advicePackages);

        allPackages.addAll(pointcut.getPackages());

        if (afterParameter != null && !afterParameter.getPackage().equals(""))
            allPackages.add(afterParameter.getPackage());

        return allPackages;
    }

    // Perform deep copy
    public Event copy() {
        Event copyOfThis = new Event(this.name);

        copyOfThis.getAdvicePackages().addAll(this.getAdvicePackages());
        copyOfThis.setDescription(this.description);
        copyOfThis.when = this.when;
        copyOfThis.setAfterParameter(this.afterParameter);
        copyOfThis.setAction(this.action);
        if (this.pointcut != null)
            copyOfThis.pointcut = this.pointcut.copy();

        return copyOfThis;
    }
}