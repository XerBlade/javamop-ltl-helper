package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Define an expression of an operator surrounded by two expressions
 *
 */


public class BinaryOpExp extends OpExp {
    public Expression left;		// Expression on left of operator
    public Expression right;	// Expression on right

    public BinaryOpExp(String operator)
    {
        super("binary", operator);
    }

    // Set the type of the left expression
    public void setLeftType(String type)
    {
        switch (type)
        {
            case "single": left = new SingleExp(); break;
            case "true": left = new LiteralExp(true); break;
            case "false": left = new LiteralExp(false); break;
            case "!":
            case "[]":
            case "<>":
            case "o":
            case "[*]":
            case "<*>":
            case "(*)": left = new UnaryOpExp(type); break;
            case "&&":
            case "||":
            case "^":
            case "=>":
            case "<=>":
            case "U":
            case "S": left = new BinaryOpExp(type); break;
            case "(  )": left = new ParenExp(); break;

            default: left = new SingleExp(); break;
        }
    }

    // Set the type of the right expression
    public void setRightType(String type)
    {
        switch (type)
        {
            case "single": right = new SingleExp(); break;
            case "true": right = new LiteralExp(true); break;
            case "false": right = new LiteralExp(false); break;
            case "!":
            case "[]":
            case "<>":
            case "o":
            case "[*]":
            case "<*>":
            case "(*)": right = new UnaryOpExp(type); break;
            case "&&":
            case "||":
            case "^":
            case "=>":
            case "<=>":
            case "U":
            case "S": right = new BinaryOpExp(type); break;
            case "(  )": right = new ParenExp(); break;

            default: right = new SingleExp(); break;
        }
    }

    @Override
    public String toLongString()
    {
        String stringRep;

        if (left != null) {
            stringRep = left.toLongString();
        }
        else
            stringRep = "null";

        stringRep += " " + this.getOperator() + " ";

        if (right != null) {
            stringRep += right.toLongString();
        }
        else
            stringRep += "null";

        return stringRep;
    }

    @Override
    public boolean hasEvent(Event event) {
        return left.hasEvent(event) || right.hasEvent(event);
    }

    @Override
    public Boolean hasNullExp() {
        return (left == null || right == null || left.hasNullExp() || right.hasNullExp());
    }

}
