package ltlAgentPkg;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 *
 * A parenthetical operator expression for pointcuts
 */
public class ParenCut extends OpCut {
    public Pointcut inner;      // The inner expression within the parentheses

    ParenCut() {
        super("paren", "(  )");
    }

    // Set the type of the inner pointcut
    public void setInnerType(String type)
    {
        switch (type)
        {
            case "call":
            case "execution":
            case "get":
            case "set":
            case "handler":
            case "staticinitialization":
            case "initialization":
            case "preinitialization":
            case "condition": inner = new ExpCut(type); break;
            case "this":
            case "target":
            case "args": inner = new ParamCut(type); break;
            case "!": inner = new UnaryOpCut(type); break;
            case "&&":
            case "||": inner = new BinaryOpCut(type); break;
            case "(  )": inner = new ParenCut(); break;
            default: inner = new ExpCut("call"); break;
        }
    }

    @Override
    public String toLongString() {
        if (inner != null)
            return "( " + inner.toLongString() + " )";
        else
            return "( null )";
    }

    @Override
    public boolean hasParameter(Parameter parameter) {
        return (inner != null) && inner.hasParameter(parameter);
    }

    @Override
    public Boolean hasNullExp() {
        return (inner == null) || inner.hasNullExp();
    }

    @Override
    public Pointcut copy() {
        ParenCut copyOfThis = new ParenCut();

        copyOfThis.inner = this.inner.copy();

        return copyOfThis;
    }

    @Override
    public ArrayList<Parameter> getAllUsedParameters() {
        ArrayList<Parameter> parameters = new ArrayList<>();

        if (inner != null)
            parameters.addAll(inner.getAllUsedParameters());

        return parameters;
    }

    @Override
    public ConcurrentSkipListSet<String> getPackages() {
        ConcurrentSkipListSet<String> requiredPackages = new ConcurrentSkipListSet<>();

        if (inner != null)
            requiredPackages.addAll(inner.getPackages());

        return requiredPackages;
    }
}
