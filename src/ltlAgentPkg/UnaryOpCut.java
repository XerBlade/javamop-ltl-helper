package ltlAgentPkg;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 */
public class UnaryOpCut extends OpCut {
    public Pointcut right;

    UnaryOpCut(String operator) {
        super("unary", operator);
    }

    // Set the type of the right pointcut
    public void setRightType(String type)
    {
        switch (type)
        {
            case "call":
            case "execution":
            case "get":
            case "set":
            case "handler":
            case "staticinitialization":
            case "initialization":
            case "preinitialization":
            case "condition": right = new ExpCut(type); break;
            case "this":
            case "target":
            case "args": right = new ParamCut(type); break;
            case "!": right = new UnaryOpCut(type); break;
            case "&&":
            case "||": right = new BinaryOpCut(type); break;
            case "(  )": right = new ParenCut(); break;
            default: right = new ExpCut("call"); break;
        }
    }

    @Override
    public String toLongString() {
        String stringRep;

        stringRep = this.getOperator() + " ";

        if (right != null) {
            stringRep += right.toLongString();
        }
        else
            stringRep += "null";

        return stringRep;
    }

    @Override
    public boolean hasParameter(Parameter parameter) {
        return (right != null) && right.hasParameter(parameter);
    }

    @Override
    public Boolean hasNullExp() {
        return (right == null || right.hasNullExp());
    }

    @Override
    public Pointcut copy() {
        UnaryOpCut copyOfThis = new UnaryOpCut(this.operator);

        copyOfThis.right = this.right.copy();

        return copyOfThis;
    }

    @Override
    public ArrayList<Parameter> getAllUsedParameters() {
        ArrayList<Parameter> parameters = new ArrayList<>();

        if (right != null)
            parameters.addAll(right.getAllUsedParameters());

        return parameters;
    }

    @Override
    public ConcurrentSkipListSet<String> getPackages() {
        ConcurrentSkipListSet<String> requiredPackages = new ConcurrentSkipListSet<>();

        requiredPackages.addAll(right.getPackages());

        return requiredPackages;
    }
}
