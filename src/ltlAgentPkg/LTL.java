package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Abstract base class for representing a single LTL formula
 *
 */



public abstract class LTL {
    private String type;	// Type of LTL

    public String getType()
    {
        return this.type;
    }

    // Constructor setting type
    public LTL(String type)
    {
        this.type = type;
    }

    // Generate string representation
    public abstract String toLongString();

    // Search for an event
    public abstract boolean hasEvent(Event event);

    // Check if LTL has any null expression
    public abstract Boolean hasNullExp();
}
