package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Abstract base class for representing an expression within an LTL formula
 *
 */


public abstract class Expression {
    private String type;		// Type of expression

    public String getType()
    {
        return this.type;
    }

    // Constructor setting type
    public Expression(String type)
    {
        this.type = type;
    }

    // Generate string representation
    public abstract String toLongString();

    // Search for an event
    public abstract boolean hasEvent(Event event);

    // Returns true if any part of the expression is null
    public abstract Boolean hasNullExp();
}
