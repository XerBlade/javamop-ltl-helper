package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Define an expression that consists of only a single predicate
 *
 */


// Expression representing a single event
public class SingleExp extends Expression {
    private Event event;	// Which event is being represented

    SingleExp()
    {
        super("single");
        this.event = new Event();
    }

    // Constructor initializing event
    public SingleExp(Event event)
    {
        super("single");
        this.event = event;
    }

    public void setEvent(Event event)
    {
        this.event = event;
    }

    public Event getEvent()
    {
        return this.event;
    }

    @Override
    public String toLongString() {
        return this.event.getName();
    }

    @Override
    public boolean hasEvent(Event event) {
        return this.event == event;
    }

    @Override
    public Boolean hasNullExp() {
        return false;
    }

}
