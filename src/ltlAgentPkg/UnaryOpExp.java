package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Define an expression of an operator followed by an expression
 *
 */


public class UnaryOpExp extends OpExp {
    public Expression right;	// Expression to right of operator

    public UnaryOpExp(String operator)
    {
        super("unary", operator);
    }

    // Set the type of the right expression
    public void setRightType(String type)
    {
        switch (type)
        {
            case "single": right = new SingleExp(); break;
            case "true": right = new LiteralExp(true); break;
            case "false": right = new LiteralExp(false); break;
            case "!":
            case "[]":
            case "<>":
            case "o":
            case "[*]":
            case "<*>":
            case "(*)": right = new UnaryOpExp(type); break;
            case "&&":
            case "||":
            case "^":
            case "=>":
            case "<=>":
            case "U":
            case "S": right = new BinaryOpExp(type); break;
            case "(  )": right = new ParenExp(); break;

            default: right = new SingleExp(); break;
        }
    }

    @Override
    public String toLongString()
    {
        String stringRep;

        stringRep = this.getOperator() + " ";

        if (right != null) {
            stringRep += right.toLongString();
        }
        else
            stringRep += "null";

        return stringRep;
    }

    @Override
    public boolean hasEvent(Event event) {
        return right.hasEvent(event);
    }

    @Override
    public Boolean hasNullExp() {
        return (right == null || right.hasNullExp());
    }

}
