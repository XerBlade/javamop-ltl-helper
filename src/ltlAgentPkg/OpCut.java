package ltlAgentPkg;

/**
 * Created by Rex Cope
 */
public abstract class OpCut extends Pointcut {
    String operator;

    OpCut(String type, String operator) {
        super(type);
        this.operator = operator;
    }

    public String getOperator() {
        return this.operator;
    }
}
