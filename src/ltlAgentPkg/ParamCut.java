package ltlAgentPkg;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 */
public class ParamCut extends Pointcut {
    private Parameter parameter;
    private String prefix;

    ParamCut(String type) {
        super("param");
        this.prefix = type;
    }

    public Parameter getParameter() {
        return this.parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public String getPrefix() {
        return this.prefix;
    }

    @Override
    public String toLongString() {
        if (parameter != null)
            return prefix + "(" + parameter.getName() + ")";
        else
            return "null";
    }

    @Override
    public boolean hasParameter(Parameter parameter) {
        return this.parameter == parameter;
    }

    @Override
    public Boolean hasNullExp() {
        return parameter == null;
    }

    @Override
    public Pointcut copy() {
        ParamCut copyOfThis = new ParamCut(this.prefix);

        copyOfThis.setParameter(this.parameter);

        return copyOfThis;
    }

    @Override
    public ArrayList<Parameter> getAllUsedParameters() {
        ArrayList<Parameter> parameters = new ArrayList<>();

        parameters.add(parameter);

        return parameters;
    }

    @Override
    public ConcurrentSkipListSet<String> getPackages() {
        ConcurrentSkipListSet<String> requiredPackage = new ConcurrentSkipListSet<>();

        if (!parameter.getPackage().equals(""))
            requiredPackage.add(parameter.getPackage());

        return requiredPackage;
    }
}
