package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Represent an LTL constructed by editor helper
 *
 */


public class HelperLTL extends LTL {
    public Expression expression;	// Expression used to construct this LTL

    public HelperLTL()
    {
        super("Helper");
    }

    // Set type of expression
    public void setExpType(String type)
    {
        switch (type)
        {
            case "single": expression = new SingleExp(); break;
            case "true": expression = new LiteralExp(true); break;
            case "false": expression = new LiteralExp(false); break;
            case "!":
            case "[]":
            case "<>":
            case "o":
            case "[*]":
            case "<*>":
            case "(*)": expression = new UnaryOpExp(type); break;
            case "&&":
            case "||":
            case "^":
            case "=>":
            case "<=>":
            case "U":
            case "S": expression = new BinaryOpExp(type); break;
            case "(  )": expression = new ParenExp(); break;

            default: expression = new SingleExp(); break;
        }
    }

    @Override
    public String toLongString()
    {
        if (expression != null) {
            return expression.toLongString();
        }
        else {
            return "null";
        }
    }

    @Override
    public boolean hasEvent(Event event) {
        return expression.hasEvent(event);
    }

    public Boolean hasNullExp() {
        return (expression == null || expression.hasNullExp());
    }
}
