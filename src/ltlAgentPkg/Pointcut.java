package ltlAgentPkg;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 */
public abstract class Pointcut {
    private String type;		// Type of expression

    public String getType() {
        return this.type;
    }

    // Constructor setting type
    public Pointcut(String type) {
        this.type = type;
    }

    // Generate string representation
    public abstract String toLongString();

    // Search for a parameter
    public abstract boolean hasParameter(Parameter parameter);

    // Returns true if any part of the expression is null
    public abstract Boolean hasNullExp();

    // Perform deep copy
    public abstract Pointcut copy();

    // Get all the parameters used in the pointcut
    public abstract ArrayList<Parameter> getAllUsedParameters();

    // Get all the packages required in the pointcut
    public abstract ConcurrentSkipListSet<String> getPackages();
}
