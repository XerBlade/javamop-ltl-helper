package ltlAgentPkg;

import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Created by Rex Cope
 *
 * In-memory representation of a single LTL file
 */
public class LTLFile {
    private ArrayList<LTLAgent> ltlAgents;  // List of LTLs stored in this file
    private ArrayList<Event> globalEvents;        // List of global events
    private String path;                    // Path to file
    private Path pathAsPath;                // Path as path to file
    private String description;             // Description of file
    private Boolean modified;               // Whether file has been modified

    public ArrayList<LTLAgent> getLtlAgents() {
        return ltlAgents;
    }

    public ArrayList<Event> getGlobalEvents() {
        return globalEvents;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }

    public Path getPathAsPath() {
        return this.pathAsPath;
    }

    public void setPathAsPath(Path path) {
        this.pathAsPath = path;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setModified(Boolean modified) {
        this.modified = modified;
    }

    public Boolean getModified() {
        return this.modified;
    }

    public LTLFile() {
        ltlAgents = new ArrayList<>();
        globalEvents = new ArrayList<>();
        modified = false;
    }

    // Check if any events have any null expressions
    public Event eventHasNullExp() {
        for (Event event : globalEvents) {
            if (event == null || event.hasNullExp()) {
                return event;
            }
        }

        return null;
    }
}
