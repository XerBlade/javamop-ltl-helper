package ltlAgentPkg;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 */
public class BinaryOpCut extends OpCut {
    public Pointcut left;
    public Pointcut right;

    BinaryOpCut(String operator) {
        super("binary", operator);
    }

    // Set the type of the right pointcut
    public void setLeftType(String type)
    {
        switch (type)
        {
            case "call":
            case "execution":
            case "get":
            case "set":
            case "handler":
            case "staticinitialization":
            case "initialization":
            case "preinitialization":
            case "condition": left = new ExpCut(type); break;
            case "this":
            case "target":
            case "args": left = new ParamCut(type); break;
            case "!": left = new UnaryOpCut(type); break;
            case "&&":
            case "||": left = new BinaryOpCut(type); break;
            case "(  )": left = new ParenCut(); break;
            default: left = new ExpCut("call"); break;
        }
    }

    // Set the type of the right pointcut
    public void setRightType(String type)
    {
        switch (type)
        {
            case "call":
            case "execution":
            case "get":
            case "set":
            case "handler":
            case "staticinitialization":
            case "initialization":
            case "preinitialization":
            case "condition": right = new ExpCut(type); break;
            case "this":
            case "target":
            case "args": right = new ParamCut(type); break;
            case "!": right = new UnaryOpCut(type); break;
            case "&&":
            case "||": right = new BinaryOpCut(type); break;
            case "(  )": right = new ParenCut(); break;
            default: right = new ExpCut("call"); break;
        }
    }

    @Override
    public String toLongString() {
        String stringRep;

        if (left != null) {
            stringRep = left.toLongString();
        }
        else
            stringRep = "null";

        stringRep += " " + this.getOperator() + " ";

        if (right != null) {
            stringRep += right.toLongString();
        }
        else
            stringRep += "null";

        return stringRep;
    }

    @Override
    public boolean hasParameter(Parameter parameter) {
        return ((left != null) && left.hasParameter(parameter)) || ((right != null) && right.hasParameter(parameter));
    }

    @Override
    public Boolean hasNullExp() {
        return (left == null || right == null || left.hasNullExp() || right.hasNullExp());
    }

    @Override
    public Pointcut copy() {
        BinaryOpCut copyOfThis = new BinaryOpCut(this.operator);

        copyOfThis.left = this.left.copy();
        copyOfThis.right = this.right.copy();

        return copyOfThis;
    }

    @Override
    public ArrayList<Parameter> getAllUsedParameters() {
        ArrayList<Parameter> parameters = new ArrayList<>();

        if (left != null)
            parameters.addAll(left.getAllUsedParameters());
        if (right != null)
            parameters.addAll(right.getAllUsedParameters());

        return parameters;
    }

    @Override
    public ConcurrentSkipListSet<String> getPackages() {
        ConcurrentSkipListSet<String> requiredPackages = new ConcurrentSkipListSet<>();

        requiredPackages.addAll(left.getPackages());
        requiredPackages.addAll(right.getPackages());

        return requiredPackages;
    }
}
