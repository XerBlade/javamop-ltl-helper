package ltlAgentPkg;

/**
 * Created by Rex Cope
 * Represents true or false
 */
public class LiteralExp extends Expression {
    private boolean value;      // Whether is true or false

    public LiteralExp(boolean v) {
        super("literal");

        value = v;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public String toLongString() {
        if (value)
            return "true";
        else
            return "false";
    }

    @Override
    public boolean hasEvent(Event event) {
        return false;
    }

    @Override
    public Boolean hasNullExp() {
        return false;
    }
}
