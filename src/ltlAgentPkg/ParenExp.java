package ltlAgentPkg;

/**
 * Created by Rex Cope
 *
 * A parenthetical operator expression for LTL formulas
 */
public class ParenExp extends OpExp {
    public Expression inner;        // The expression inside the parentheses

    ParenExp() {
        super("paren", "(  )");
    }

    // Set the type of the inner expression
    public void setInnerType(String type)
    {
        switch (type)
        {
            case "single": inner = new SingleExp(); break;
            case "true": inner = new LiteralExp(true); break;
            case "false": inner = new LiteralExp(false); break;
            case "!":
            case "[]":
            case "<>":
            case "o":
            case "[*]":
            case "<*>":
            case "(*)": inner = new UnaryOpExp(type); break;
            case "&&":
            case "||":
            case "^":
            case "=>":
            case "<=>":
            case "U":
            case "S": inner = new BinaryOpExp(type); break;
            case "(  )": inner = new ParenExp(); break;

            default: inner = new SingleExp(); break;
        }
    }

    @Override
    public String toLongString() {
        if (inner != null)
            return "( " + inner.toLongString() + " )";
        else
            return "( null )";
    }

    @Override
    public boolean hasEvent(Event event) {
        return inner.hasEvent(event);
    }

    @Override
    public Boolean hasNullExp() {
        return (inner == null || inner.hasNullExp());
    }
}
