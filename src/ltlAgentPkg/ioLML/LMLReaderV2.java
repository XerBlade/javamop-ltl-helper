package ltlAgentPkg.ioLML;

import ltlAgentPkg.AInstanceVar;
import ltlAgentPkg.LTLAgent;
import ltlAgentPkg.LTLFile;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Rex Cope
 */
@SuppressWarnings("WeakerAccess")
class LMLReaderV2 extends LMLReaderV1 {

    // Read LTL List from XML file at path
    @Override
    public LTLFile readXML(Element root) throws JDOMException, IOException {
        file = new LTLFile();
        file.getLtlAgents().clear();

        // Get all global events into globalEvents
        Element gEvents = root.getChild("events");

        readEvents(gEvents, file.getGlobalEvents());

        // Get file description
        file.setDescription(root.getChildText("description"));

        // Get all agents into list
        for (Element agent : root.getChildren("agent"))
        {
            LTLAgent ltlAgent = new LTLAgent();

            // Get name of agent
            ltlAgent.setName(agent.getChildText("name"));

            // Get description
            ltlAgent.setDescription(agent.getChildText("description"));

            // Get modifier
            ltlAgent.setModifier(agent.getChildText("modifier"));

            // Get static parameters
            readParameters(agent.getChild("parameters"), ltlAgent.parameters);

            // Get aspect instance variables
            readInstanceVars(agent.getChild("variables"), ltlAgent.instanceVars);

            // Get events
            readEvents(agent.getChild("events"), ltlAgent.events);
            events = ltlAgent.events;

            // Get LTL
            String type = agent.getChild("ltl").getAttributeValue("type");
            ltlAgent.setType(type);
            readLTL(agent.getChild("ltl"), ltlAgent.ltl);

            // Get property handler
            readPropertyHandler(agent.getChild("handler"), ltlAgent);

            file.getLtlAgents().add(ltlAgent);
        }

        return file;
    }

    protected void readInstanceVars(Element variablesElement, ArrayList<AInstanceVar> instanceVars) {
        instanceVars.clear();
        if (variablesElement != null) {
            for (Element variableElement : variablesElement.getChildren("variable")) {
                AInstanceVar variable = new AInstanceVar();
                readInstanceVar(variableElement, variable);
                instanceVars.add(variable);
            }
        }
    }

    protected void readInstanceVar(Element variableElement, AInstanceVar variable) {
        if (variableElement != null) {
            variable.setType(variableElement.getChildText("type"));
            variable.setId(variableElement.getChildText("name"));
            variable.setInitializer(variableElement.getChildText("initializer"));
            variable.setDescription(variableElement.getChildText("description"));
            variable.setRequiredPackage(variableElement.getChildText("package"));
        }
    }
}
