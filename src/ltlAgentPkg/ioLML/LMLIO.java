package ltlAgentPkg.ioLML;

import ltlAgentPkg.LTLFile;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;

/**
 * Created by Rex Cope
 */
public class LMLIO {
    // Write LTL List as XML file at path
    public static void writeXML(LTLFile ltlFile, String path) throws IOException, JDOMException {
        LMLWriter writer = new LMLWriter();
        writer.writeXML(ltlFile, path);
    }

    // Read LTL List from XML file at path
    public static LTLFile readXML(String path) throws JDOMException, IOException {
        LMLReader reader;

        SAXBuilder builder = new SAXBuilder();


        // Parse the file into a JDOM document
        Document readDoc = builder.build(new File(path));

        // Get root element
        Element root = readDoc.getRootElement();

        // Get version
        String version;
        Element versionElement = root.getChild("version");
        if (versionElement == null)
            version = "1";
        else
            version = versionElement.getText();

        switch (version) {
            case "1": reader = new LMLReaderV1(); break;
            case "2": reader = new LMLReaderV2(); break;
            default: throw new JDOMException("Unsupported format");
        }

        LTLFile file = reader.readXML(root);

        file.setPath(path);

        return file;
    }
}
