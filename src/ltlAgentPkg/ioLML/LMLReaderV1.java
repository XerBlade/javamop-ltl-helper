package ltlAgentPkg.ioLML;

import ltlAgentPkg.*;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

/**
 * Created by Rex Cope
 */
@SuppressWarnings("WeakerAccess")
class LMLReaderV1 extends LMLReader {
    LTLFile file;        // Storage container for file data
    ArrayList<Event> events = new ArrayList<>();

    // Read LTL List from XML file at path
    @Override
    public LTLFile readXML(Element root) throws JDOMException, IOException {
        file = new LTLFile();
        file.getLtlAgents().clear();

        // Get all global events into globalEvents
        Element gEvents = root.getChild("events");

        readEvents(gEvents, file.getGlobalEvents());

        // Get file description
        file.setDescription(root.getChildText("description"));

        // Get all agents into list
        for (Element agent : root.getChildren("agent"))
        {
            LTLAgent ltlAgent = new LTLAgent();

            // Get name of agent
            ltlAgent.setName(agent.getChildText("name"));

            // Get description
            ltlAgent.setDescription(agent.getChildText("description"));

            // Get modifier
            ltlAgent.setModifier(agent.getChildText("modifier"));

            // Get static parameters
            readParameters(agent.getChild("parameters"), ltlAgent.parameters);

            // Get events
            readEvents(agent.getChild("events"), ltlAgent.events);
            events = ltlAgent.events;

            // Get LTL
            String type = agent.getChild("ltl").getAttributeValue("type");
            ltlAgent.setType(type);
            readLTL(agent.getChild("ltl"), ltlAgent.ltl);

            // Get property handler
            readPropertyHandler(agent.getChild("handler"), ltlAgent);

            file.getLtlAgents().add(ltlAgent);
        }

        return file;
    }

    void readPropertyHandler(Element handler, LTLAgent ltlAgent) {
        if (handler != null){
            if (handler.getChild("condition").getChildText("value").equals("true"))
                ltlAgent.setMatchFailCondition(true);
            else
                ltlAgent.setMatchFailCondition(false);

            ltlAgent.setPropertyHandler(handler.getChildText("content"));

            readPackages(handler.getChild("packages"), ltlAgent.getPropertyPackages());
        }
    }

    void readPackages(Element packages, ConcurrentSkipListSet<String> setPackages) {
        setPackages.clear();
        if (packages != null) {
            setPackages.addAll(packages.getChildren("package").stream().map(Element::getText).collect(Collectors.toList()));
        }
    }

    void readParameters(Element parameters, ArrayList<Parameter> parametersList) {
        parametersList.clear();
        if (parameters != null) {
            for (Element parameter : parameters.getChildren("parameter")) {
                Parameter newParameter = new Parameter();
                readParameter(parameter, newParameter);
                parametersList.add(newParameter);
            }
        }
    }

    void readParameter(Element parameter, Parameter ltlAgentParameter) {
        if (parameter != null) {
            ltlAgentParameter.setType(parameter.getChildText("type"));
            ltlAgentParameter.setName(parameter.getChildText("name"));
            ltlAgentParameter.setDescription(parameter.getChildText("description"));
            ltlAgentParameter.setPackage(parameter.getChildText("package"));
        }
    }

    // Create LTL based on XML
    void readLTL(Element ltlElement, LTL ltl)
    {
        String type = ltl.getType();

        // Create LTL for correct type
        switch (type)
        {
            case "Helper":
                readHelperLTL(ltlElement, (HelperLTL) ltl);
                break;
            case "Custom":
                readCustomLTL(ltlElement, (CustomLTL) ltl);
                break;
        }
    }

    // Create CustomLTL based on XML
    void readHelperLTL(Element ltlElement, HelperLTL ltl)
    {
        // Get expression and type
        Element expElement = ltlElement.getChild("expression");
        String type = expElement.getAttributeValue("type");

        switch (type)
        {
            case "single":
                ltl.setExpType("single");
                readSingleExp(expElement, (SingleExp) ltl.expression);
                break;
            case "literal":
                ltl.setExpType(expElement.getChildText("value"));
                break;
            case "unary":
                ltl.setExpType(expElement.getChildText("operator"));
                readUnaryExp(expElement, (UnaryOpExp) ltl.expression);
                break;
            case "binary":
                ltl.setExpType(expElement.getChildText("operator"));
                readBinaryExp(expElement, (BinaryOpExp) ltl.expression);
                break;
            case "paren":
                ltl.setExpType("(  )");
                readParenExp(expElement, (ParenExp) ltl.expression);
                break;
        }
    }

    // Create SingleExp from XML element
    void readSingleExp(Element expElement, SingleExp expression)
    {
        // Get event name
        String eventName = expElement.getChildText("event");

        // List of all events that can be used
        ArrayList<Event> allEvents = new ArrayList<>();
        allEvents.addAll(file.getGlobalEvents());
        allEvents.addAll(events);

        // Look for event with that name
        for (Event event : allEvents)
        {
            if (event.getName().equals(eventName))
            {
                expression.setEvent(event);
                break;
            }
        }
    }

    // Create ParenExp from XML element
    void readParenExp(Element expElement, ParenExp expression) {
        // Get inner expression
        Element inner = expElement.getChild("inner");
        String innerType = inner.getAttributeValue("type");

        switch (innerType) {
            case "single":
                expression.setInnerType("single");
                readSingleExp(inner, (SingleExp) expression.inner);
                break;
            case "literal":
                expression.setInnerType(inner.getChildText("value"));
                break;
            case "unary":
                expression.setInnerType(inner.getChildText("operator"));
                readUnaryExp(inner, (UnaryOpExp) expression.inner);
                break;
            case "binary":
                expression.setInnerType(inner.getChildText("operator"));
                readBinaryExp(inner, (BinaryOpExp) expression.inner);
                break;
            case "paren":
                expression.setInnerType("(  )");
                readParenExp(inner, (ParenExp) expression.inner);
                break;
        }
    }

    // Create UnaryOpExp from XML element
    void readUnaryExp(Element expElement, UnaryOpExp expression)
    {
        // Get right side expression
        Element right = expElement.getChild("right");
        String rightType = right.getAttributeValue("type");

        switch (rightType)
        {
            case "single":
                expression.setRightType("single");
                readSingleExp(right, (SingleExp) expression.right);
                break;
            case "literal":
                expression.setRightType(right.getChildText("value"));
                break;
            case "unary":
                expression.setRightType(right.getChildText("operator"));
                readUnaryExp(right, (UnaryOpExp) expression.right);
                break;
            case "binary":
                expression.setRightType(right.getChildText("operator"));
                readBinaryExp(right, (BinaryOpExp) expression.right);
                break;
            case "paren":
                expression.setRightType("(  )");
                readParenExp(right, (ParenExp) expression.right);
                break;
        }
    }

    // Create BinaryOpExp from XML element
    void readBinaryExp(Element expElement, BinaryOpExp expression)
    {
        // Get left and right side expression
        Element left = expElement.getChild("left");
        String leftType = left.getAttributeValue("type");
        Element right = expElement.getChild("right");
        String rightType = right.getAttributeValue("type");

        switch (leftType)
        {
            case "single":
                expression.setLeftType("single");
                readSingleExp(left, (SingleExp) expression.left);
                break;
            case "literal":
                expression.setLeftType(left.getChildText("value"));
                break;
            case "unary":
                expression.setLeftType(left.getChildText("operator"));
                readUnaryExp(left, (UnaryOpExp) expression.left);
                break;
            case "binary":
                expression.setLeftType(left.getChildText("operator"));
                readBinaryExp(left, (BinaryOpExp) expression.left);
                break;
            case "paren":
                expression.setLeftType("(  )");
                readParenExp(left, (ParenExp) expression.left);
                break;
        }

        switch (rightType)
        {
            case "single":
                expression.setRightType("single");
                readSingleExp(right, (SingleExp) expression.right);
                break;
            case "literal":
                expression.setRightType(right.getChildText("value"));
                break;
            case "unary":
                expression.setRightType(right.getChildText("operator"));
                readUnaryExp(right, (UnaryOpExp) expression.right);
                break;
            case "binary":
                expression.setRightType(right.getChildText("operator"));
                readBinaryExp(right, (BinaryOpExp) expression.right);
                break;
            case "paren":
                expression.setRightType("(  )");
                readParenExp(right, (ParenExp) expression.right);
                break;
        }
    }

    // Create CustomLTL based on XML
    void readCustomLTL(Element ltlElement, CustomLTL ltl)
    {
        ltl.setContent(ltlElement.getChildText("content"));
    }

    // Create events based on XML
    void readEvents(Element events, ArrayList<Event> eventsList)
    {
        eventsList.clear();
        for (Element event : events.getChildren("event"))
        {
            Event newEvent = new Event(event.getChildText("name"));
            readParameters(event.getChild("parameters"), newEvent.parameters);

            readEventMapping(event.getChild("mapping"), newEvent);

            newEvent.setDescription(event.getChildText("description"));

            readPackages(event.getChild("packages"), newEvent.getAdvicePackages());

            eventsList.add(newEvent);
        }
    }

    void readEventMapping(Element mapping, Event newEvent) {
        Element when = mapping.getChild("when");
        if (when != null) {
            newEvent.setWhen(when.getAttributeValue("type"));
            if (newEvent.getWhen().equals("after returning") || newEvent.getWhen().equals("after throwing")) {

                Parameter afterParameter = new Parameter();
                readParameter(when.getChild("parameter"), afterParameter);
                newEvent.setAfterParameter(afterParameter);

            }
        }
        Element pointcut = mapping.getChild("pointcut");
        if (pointcut != null)
            readPointcut(pointcut, newEvent);

        Element action = mapping.getChild("action");
        if (action != null)
            newEvent.setAction(action.getChildText("content"));
    }

    void readPointcut(Element pointcutElement, Event event) {
        // Get pointcut and type
        String type = pointcutElement.getAttributeValue("type");

        if (type != null) {
            switch (type) {
                case "exp":
                    event.setCutType(pointcutElement.getChildText("cut"));
                    readExpCut(pointcutElement, (ExpCut) event.pointcut);
                    break;
                case "param":
                    event.setCutType(pointcutElement.getChildText("cut"));
                    readParamCut(pointcutElement, (ParamCut) event.pointcut);
                    break;
                case "unary":
                    event.setCutType(pointcutElement.getChildText("operator"));
                    readUnaryCut(pointcutElement, (UnaryOpCut) event.pointcut);
                    break;
                case "binary":
                    event.setCutType(pointcutElement.getChildText("operator"));
                    readBinaryCut(pointcutElement, (BinaryOpCut) event.pointcut);
                    break;
                case "paren":
                    event.setCutType("(  )");
                    readParenCut(pointcutElement, (ParenCut) event.pointcut);
                    break;
            }
        }
    }

    void readParenCut(Element pointcutElement, ParenCut pointcut) {
        // Get inner expression
        Element innerElement = pointcutElement.getChild("innercut");

        if (innerElement != null) {
            String innerType = innerElement.getAttributeValue("type");
            switch (innerType) {
                case "exp":
                    pointcut.setInnerType(innerElement.getChildText("cut"));
                    readExpCut(innerElement, (ExpCut) pointcut.inner);
                    break;
                case "param":
                    pointcut.setInnerType(innerElement.getChildText("cut"));
                    readParamCut(innerElement, (ParamCut) pointcut.inner);
                    break;
                case "unary":
                    pointcut.setInnerType(innerElement.getChildText("operator"));
                    readUnaryCut(innerElement, (UnaryOpCut) pointcut.inner);
                    break;
                case "binary":
                    pointcut.setInnerType(innerElement.getChildText("operator"));
                    readBinaryCut(innerElement, (BinaryOpCut) pointcut.inner);
                    break;
                case "paren":
                    pointcut.setInnerType("(  )");
                    readParenCut(innerElement, (ParenCut) pointcut.inner);
                    break;
            }
        }
    }

    void readBinaryCut(Element pointcutElement, BinaryOpCut pointcut) {
        // Get left side expression
        Element leftElement = pointcutElement.getChild("leftcut");
        // Get right side expression
        Element rightElement = pointcutElement.getChild("rightcut");
        String cut;

        if (leftElement != null) {
            String leftType = leftElement.getAttributeValue("type");
            switch (leftType) {
                case "exp":
                    cut = leftElement.getChildText("cut");
                    if (cut != null) {
                        pointcut.setLeftType(leftElement.getChildText("cut"));
                        readExpCut(leftElement, (ExpCut) pointcut.left);
                    }
                    break;
                case "param":
                    cut = leftElement.getChildText("cut");
                    if (cut != null) {
                        pointcut.setLeftType(leftElement.getChildText("cut"));
                        readParamCut(leftElement, (ParamCut) pointcut.left);
                    }
                    break;
                case "unary":
                    pointcut.setLeftType(leftElement.getChildText("operator"));
                    readUnaryCut(leftElement, (UnaryOpCut) pointcut.left);
                    break;
                case "binary":
                    pointcut.setLeftType(leftElement.getChildText("operator"));
                    readBinaryCut(leftElement, (BinaryOpCut) pointcut.left);
                    break;
                case "paren":
                    pointcut.setLeftType("(  )");
                    readParenCut(leftElement, (ParenCut) pointcut.left);
                    break;
                default:
                    break;
            }
        }

        if (rightElement != null) {
            String rightType = rightElement.getAttributeValue("type");
            switch (rightType) {
                case "exp":
                    cut = rightElement.getChildText("cut");
                    if (cut != null) {
                        pointcut.setRightType(rightElement.getChildText("cut"));
                        readExpCut(rightElement, (ExpCut) pointcut.right);
                    }
                    break;
                case "param":
                    cut = rightElement.getChildText("cut");
                    if (cut != null) {
                        pointcut.setRightType(rightElement.getChildText("cut"));
                        readParamCut(rightElement, (ParamCut) pointcut.right);
                    }
                    break;
                case "unary":
                    pointcut.setRightType(rightElement.getChildText("operator"));
                    readUnaryCut(rightElement, (UnaryOpCut) pointcut.right);
                    break;
                case "binary":
                    pointcut.setRightType(rightElement.getChildText("operator"));
                    readBinaryCut(rightElement, (BinaryOpCut) pointcut.right);
                    break;
                case "paren":
                    pointcut.setRightType("(  )");
                    readParenCut(rightElement, (ParenCut) pointcut.right);
                    break;
            }
        }
    }

    void readUnaryCut(Element pointcutElement, UnaryOpCut pointcut) {
        // Get right side expression
        Element rightElement = pointcutElement.getChild("rightcut");

        if (rightElement != null) {
            String rightType = rightElement.getAttributeValue("type");
            switch (rightType) {
                case "exp":
                    pointcut.setRightType(rightElement.getChildText("cut"));
                    readExpCut(rightElement, (ExpCut) pointcut.right);
                    break;
                case "param":
                    pointcut.setRightType(rightElement.getChildText("cut"));
                    readParamCut(rightElement, (ParamCut) pointcut.right);
                    break;
                case "unary":
                    pointcut.setRightType(rightElement.getChildText("operator"));
                    readUnaryCut(rightElement, (UnaryOpCut) pointcut.right);
                    break;
                case "binary":
                    pointcut.setRightType(rightElement.getChildText("operator"));
                    readBinaryCut(rightElement, (BinaryOpCut) pointcut.right);
                    break;
                case "paren":
                    pointcut.setRightType("(  )");
                    readParenCut(rightElement, (ParenCut) pointcut.right);
                    break;
            }
        }
    }

    void readExpCut(Element pointcutElement, ExpCut pointcut) {
        if (pointcutElement != null) {
            String content = pointcutElement.getChildText("content");
            pointcut.setContent(content);
        }
    }

    void readParamCut(Element pointcutElement, ParamCut pointcut) {

        Parameter newParameter = new Parameter();
        readParameter(pointcutElement.getChild("parameter"), newParameter);
        pointcut.setParameter(newParameter);

    }
}
