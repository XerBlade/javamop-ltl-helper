package ltlAgentPkg.ioLML;

import ltlAgentPkg.*;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Text;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 */
class LMLWriter {
    @SuppressWarnings("FieldCanBeLocal")
    private final int currentVersion = 2;

    // Write LTL List as XML file at path
    void writeXML(LTLFile ltlFile, String path) throws IOException, JDOMException {
        // Create new JDom document
        Document doc = new Document();

        // Create element named list and make it the root
        Element root = new Element("list");
        doc.setRootElement(root);

        // Set version number of the LML format
        Element version = new Element("version");
        version.setText(String.valueOf(currentVersion));
        root.addContent(version);

        // For all elements in globalEvents, make a new event
        Element gEvents = new Element("events");
        writeEvents(gEvents, ltlFile.getGlobalEvents());
        root.addContent(gEvents);

        // Make file description
        Element gDescription = new Element("description");
        gDescription.addContent(new Text(ltlFile.getDescription()));
        root.addContent(gDescription);

        // For all elements in list, make a new Agent in List
        for (LTLAgent ltlAgent : ltlFile.getLtlAgents())
        {
            Element agent = new Element("agent");

            // Get name of agent
            Element name = new Element("name");
            name.addContent(new Text(ltlAgent.getName()));

            // Get description
            Element description = new Element("description");
            description.addContent(new Text(ltlAgent.getDescription()));

            // Get modifier
            Element modifier = new Element("modifier");
            modifier.addContent(new Text(ltlAgent.getModifier()));

            // Get parameters
            Element parameters = new Element("parameters");
            writeParameters(parameters, ltlAgent.parameters);

            // Get aspect instance variables
            Element instanceVars = new Element("variables");
            writeInstanceVars(instanceVars, ltlAgent.instanceVars);

            // Get events
            Element events = new Element("events");
            writeEvents(events, ltlAgent.events);

            // Get LTL
            Element ltl = new Element("ltl");
            writeLTL(ltl, ltlAgent);

            // Get property handler
            Element handler = new Element("handler");
            writePropertyHandler(handler, ltlAgent);


            agent.addContent(name);
            agent.addContent(description);
            agent.addContent(modifier);
            agent.addContent(parameters);
            agent.addContent(instanceVars);
            agent.addContent(events);
            agent.addContent(ltl);
            agent.addContent(handler);

            root.addContent(agent);
        }

        // Add formatted indents
        XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());

        // Write XML to file
        xmlOutput.output(doc, new FileOutputStream(new File(path)));
    }

    private void writeInstanceVars(Element instanceVarsElement, ArrayList<AInstanceVar> instanceVars) {
        for (AInstanceVar currentVariable : instanceVars) {
            Element variableElement = new Element("variable");
            writeInstanceVar(variableElement, currentVariable);
            instanceVarsElement.addContent(variableElement);
        }
    }

    private void writeInstanceVar(Element variableElement, AInstanceVar variable) {
        Element type = new Element("type");
        Element name = new Element("name");
        Element initializer = new Element("initializer");
        Element description = new Element("description");
        Element requiredPackage = new Element("package");

        type.addContent(new Text(variable.getType()));
        name.addContent(new Text(variable.getId()));
        initializer.addContent(new Text(variable.getInitializer()));
        description.addContent(new Text(variable.getDescription()));
        requiredPackage.addContent(new Text(variable.getRequiredPackage()));

        variableElement.addContent(type);
        variableElement.addContent(name);
        variableElement.addContent(initializer);
        variableElement.addContent(description);
        variableElement.addContent(requiredPackage);
    }

    private void writePackages(Element packages, ConcurrentSkipListSet<String> setPackages) {
        for (String currentPackage : setPackages) {
            Element thisPackage = new Element("package");
            thisPackage.addContent(new Text(currentPackage));
            packages.addContent(thisPackage);
        }
    }

    private void writePropertyHandler(Element handler, LTLAgent ltlAgent) {
        Element condition = new Element("condition");
        Element content = new Element("content");
        Element packages = new Element("packages");

        Element value = new Element("value");
        if (ltlAgent.getMatchFailCondition())
            value.setText("true");
        else
            value.setText("false");
        condition.addContent(value);

        content.setText(ltlAgent.getPropertyHandler());


        // Get packages for property handler
        writePackages(packages, ltlAgent.getPropertyPackages());

        handler.addContent(condition);
        handler.addContent(content);
        handler.addContent(packages);
    }

    private void writeParameters(Element parameters, ArrayList<Parameter> parametersList) {
        if (parametersList != null) {
            // Create parameters for all parameters in list
            for (Parameter theParameter : parametersList) {
                Element parameter = new Element("parameter");
                writeParameter(parameter, theParameter);
                parameters.addContent(parameter);
            }
        }
    }

    private void writeParameter(Element parameter, Parameter ltlAgentParameter) {
        Element type = new Element("type");
        Element name = new Element("name");
        Element description = new Element("description");
        Element requiredPackage = new Element("package");

        type.addContent(new Text(ltlAgentParameter.getType()));
        name.addContent(new Text(ltlAgentParameter.getName()));
        description.addContent(new Text(ltlAgentParameter.getDescription()));
        requiredPackage.addContent(new Text(ltlAgentParameter.getPackage()));

        parameter.addContent(type);
        parameter.addContent(name);
        parameter.addContent(description);
        parameter.addContent(requiredPackage);
    }

    // Create XML list of events based on list of events
    private void writeEvents(Element events, ArrayList<Event> eventsList)
    {
        // Create events for all events in list
        for (Event theEvent : eventsList)
        {
            Element event = new Element("event");
            Element name = new Element("name");
            Element parameters = new Element("parameters");
            Element mapping = new Element("mapping");
            Element description = new Element("description");
            Element packages = new Element("packages");

            name.addContent(new Text(theEvent.getName()));
            writeParameters(parameters, theEvent.parameters);

            writeEventMapping(mapping, theEvent);

            writePackages(packages, theEvent.getAdvicePackages());

            description.addContent(new Text(theEvent.getDescription()));

            event.addContent(name);
            event.addContent(parameters);
            event.addContent(mapping);
            event.addContent(description);
            event.addContent(packages);
            events.addContent(event);
        }
    }

    private void writeEventMapping(Element mapping, Event theEvent) {

        Element when = new Element("when");
        Element pointcut = new Element("pointcut");
        Element action = new Element("action");

        when.setAttribute("type", theEvent.getWhen());
        if (theEvent.getWhen().equals("after returning") || theEvent.getWhen().equals("after throwing")) {
            Element parameter = new Element("parameter");
            writeParameter(parameter, theEvent.getAfterParameter());
            when.addContent(parameter);
        }
        writePointcut(pointcut, theEvent.pointcut);

        Element content = new Element("content");
        content.addContent(new Text(theEvent.getAction()));
        action.addContent(content);

        mapping.addContent(when);
        mapping.addContent(pointcut);
        mapping.addContent(action);
    }

    private void writePointcut(Element pointcutElement, Pointcut pointcut) {
        if (pointcut == null)
            return;

        String type = pointcut.getType();
        pointcutElement.setAttribute("type", type);

        // Create elements for expression type
        switch (type)
        {
            case "exp":
                writeExpCut(pointcutElement, (ExpCut) pointcut);
                break;
            case "param":
                writeParamCut(pointcutElement, (ParamCut) pointcut);
                break;
            case "unary":
                writeUnaryCut(pointcutElement, (UnaryOpCut) pointcut);
                break;
            case "binary":
                writeBinaryCut(pointcutElement, (BinaryOpCut) pointcut);
                break;
            case "paren":
                writeParenCut(pointcutElement, (ParenCut) pointcut);
                break;
        }
    }

    private void writeParenCut(Element pointcutElement, ParenCut pointcut) {
        if (pointcut.inner == null)
            return;

        Element inner = new Element("innercut");

        String innerType = pointcut.inner.getType();
        inner.setAttribute("type", innerType);

        switch (innerType)
        {
            case "exp":
                writeExpCut(inner, (ExpCut) pointcut.inner);
                break;
            case "param":
                writeParamCut(inner, (ParamCut) pointcut.inner);
                break;
            case "unary":
                writeUnaryCut(inner, (UnaryOpCut) pointcut.inner);
                break;
            case "binary":
                writeBinaryCut(inner, (BinaryOpCut) pointcut.inner);
                break;
            case "paren":
                writeParenCut(inner, (ParenCut) pointcut.inner);
                break;
        }

        pointcutElement.addContent(inner);
    }

    private void writeBinaryCut(Element pointcutElement, BinaryOpCut pointcut) {
        if (pointcut.right == null || pointcut.left == null)
            return;

        Element left = new Element("leftcut");
        Element operator = new Element("operator");
        Element right = new Element("rightcut");

        operator.addContent(new Text(pointcut.getOperator()));

        String leftType = pointcut.left.getType();
        left.setAttribute("type", leftType);
        String rightType = pointcut.right.getType();
        right.setAttribute("type", rightType);

        switch (leftType)
        {
            case "exp":
                writeExpCut(left, (ExpCut) pointcut.left);
                break;
            case "param":
                writeParamCut(left, (ParamCut) pointcut.left);
                break;
            case "unary":
                writeUnaryCut(left, (UnaryOpCut) pointcut.left);
                break;
            case "binary":
                writeBinaryCut(left, (BinaryOpCut) pointcut.left);
                break;
            case "paren":
                writeParenCut(left, (ParenCut) pointcut.left);
                break;
        }

        switch (rightType)
        {
            case "exp":
                writeExpCut(right, (ExpCut) pointcut.right);
                break;
            case "param":
                writeParamCut(right, (ParamCut) pointcut.right);
                break;
            case "unary":
                writeUnaryCut(right, (UnaryOpCut) pointcut.right);
                break;
            case "binary":
                writeBinaryCut(right, (BinaryOpCut) pointcut.right);
                break;
            case "paren":
                writeParenCut(right, (ParenCut) pointcut.right);
                break;
        }

        pointcutElement.addContent(left);
        pointcutElement.addContent(operator);
        pointcutElement.addContent(right);
    }

    private void writeUnaryCut(Element pointcutElement, UnaryOpCut pointcut) {
        if (pointcut.right == null)
            return;

        Element operator = new Element("operator");
        Element right = new Element("rightcut");

        operator.addContent(new Text(pointcut.getOperator()));

        String rightType = pointcut.right.getType();
        right.setAttribute("type", rightType);

        switch (rightType)
        {
            case "exp":
                writeExpCut(right, (ExpCut) pointcut.right);
                break;
            case "param":
                writeParamCut(right, (ParamCut) pointcut.right);
                break;
            case "unary":
                writeUnaryCut(right, (UnaryOpCut) pointcut.right);
                break;
            case "binary":
                writeBinaryCut(right, (BinaryOpCut) pointcut.right);
                break;
            case "paren":
                writeParenCut(right, (ParenCut) pointcut.right);
                break;
        }

        pointcutElement.addContent(operator);
        pointcutElement.addContent(right);
    }

    private void writeParamCut(Element pointcutElement, ParamCut pointcut) {
        Element cut = new Element("cut");
        Element parameter = new Element("parameter");
        cut.addContent(new Text(pointcut.getPrefix()));
        writeParameter(parameter, pointcut.getParameter());
        pointcutElement.addContent(cut);
        pointcutElement.addContent(parameter);
    }

    private void writeExpCut(Element pointcutElement, ExpCut pointcut) {
        Element cut = new Element("cut");
        Element content = new Element("content");
        cut.addContent(new Text(pointcut.getPrefix()));
        content.addContent(new Text(pointcut.getContent()));
        pointcutElement.addContent(cut);
        pointcutElement.addContent(content);
    }

    // Create XML based on LTL
    private void writeLTL(Element ltl, LTLAgent ltlAgent) throws JDOMException {
        // Get type of ltl
        String type = ltlAgent.ltl.getType();
        ltl.setAttribute("type", type);

        // Create elements for LTL of type
        switch (type)
        {
            case "Helper":
                writeHelperLTL(ltl, (HelperLTL) ltlAgent.ltl);
                break;
            case "Custom":
                writeCustomLTL(ltl, (CustomLTL) ltlAgent.ltl);
                break;
        }
    }

    // Create XML based on HelperLTL
    private void writeHelperLTL(Element ltl, HelperLTL helperLTL) throws JDOMException {
        // Create element for expression
        Element expression = new Element("expression");
        String type = helperLTL.expression.getType();
        expression.setAttribute("type", type);

        // Create elements for expression type
        switch (type)
        {
            case "single":
                writeSingleExp(expression, (SingleExp) helperLTL.expression);
                break;
            case "literal":
                writeLiteralExp(expression, (LiteralExp) helperLTL.expression);
                break;
            case "unary":
                writeUnaryExp(expression, (UnaryOpExp) helperLTL.expression);
                break;
            case "binary":
                writeBinaryExp(expression, (BinaryOpExp) helperLTL.expression);
                break;
            case "paren":
                writeParenExp(expression, (ParenExp) helperLTL.expression);
                break;
        }

        ltl.addContent(expression);
    }

    private void writeLiteralExp(Element expression, LiteralExp literalExp) {
        Element value = new Element("value");

        if (literalExp.getValue())
            value.addContent(new Text("true"));
        else
            value.addContent(new Text("false"));

        expression.addContent(value);
    }

    private void writeSingleExp(Element expression, SingleExp singleExp)
    {
        Element event = new Element("event");
        event.addContent(new Text(singleExp.getEvent().getName()));
        expression.addContent(event);
    }

    // Create element for LTL formula parenthetical expression
    private void writeParenExp(Element expression, ParenExp parenExp) throws JDOMException {
        if (parenExp.inner == null)
            throw new JDOMException("Unable to save null expression");

        Element inner = new Element("inner");

        String innerType = parenExp.inner.getType();
        inner.setAttribute("type", innerType);

        switch (innerType)
        {
            case "single":
                writeSingleExp(inner, (SingleExp) parenExp.inner);
                break;
            case "literal":
                writeLiteralExp(inner, (LiteralExp) parenExp.inner);
                break;
            case "unary":
                writeUnaryExp(inner, (UnaryOpExp) parenExp.inner);
                break;
            case "binary":
                writeBinaryExp(inner, (BinaryOpExp) parenExp.inner);
                break;
            case "paren":
                writeParenExp(inner, (ParenExp) parenExp.inner);
                break;
        }

        expression.addContent(inner);
    }

    private void writeUnaryExp(Element expression, UnaryOpExp unaryExp) throws JDOMException {
        if (unaryExp.right == null)
            throw new JDOMException("Unable to save null expression");

        Element operator = new Element("operator");
        Element right = new Element("right");

        operator.addContent(new Text(unaryExp.getOperator()));

        String rightType = unaryExp.right.getType();
        right.setAttribute("type", rightType);

        switch (rightType)
        {
            case "single":
                writeSingleExp(right, (SingleExp) unaryExp.right);
                break;
            case "literal":
                writeLiteralExp(right, (LiteralExp) unaryExp.right);
                break;
            case "unary":
                writeUnaryExp(right, (UnaryOpExp) unaryExp.right);
                break;
            case "binary":
                writeBinaryExp(right, (BinaryOpExp) unaryExp.right);
                break;
            case "paren":
                writeParenExp(right, (ParenExp) unaryExp.right);
                break;
        }

        expression.addContent(operator);
        expression.addContent(right);
    }

    private void writeBinaryExp(Element expression, BinaryOpExp binaryExp) throws JDOMException {
        if (binaryExp.right == null || binaryExp.left == null)
            throw new JDOMException("Unable to save null expression");

        Element left = new Element("left");
        Element operator = new Element("operator");
        Element right = new Element("right");

        operator.addContent(new Text(binaryExp.getOperator()));

        String leftType = binaryExp.left.getType();
        left.setAttribute("type", leftType);
        String rightType = binaryExp.right.getType();
        right.setAttribute("type", rightType);

        switch (leftType)
        {
            case "single":
                writeSingleExp(left, (SingleExp) binaryExp.left);
                break;
            case "literal":
                writeLiteralExp(left, (LiteralExp) binaryExp.left);
                break;
            case "unary":
                writeUnaryExp(left, (UnaryOpExp) binaryExp.left);
                break;
            case "binary":
                writeBinaryExp(left, (BinaryOpExp) binaryExp.left);
                break;
            case "paren":
                writeParenExp(left, (ParenExp) binaryExp.left);
                break;
        }

        switch (rightType)
        {
            case "single":
                writeSingleExp(right, (SingleExp) binaryExp.right);
                break;
            case "literal":
                writeLiteralExp(right, (LiteralExp) binaryExp.right);
                break;
            case "unary":
                writeUnaryExp(right, (UnaryOpExp) binaryExp.right);
                break;
            case "binary":
                writeBinaryExp(right, (BinaryOpExp) binaryExp.right);
                break;
            case "paren":
                writeParenExp(right, (ParenExp) binaryExp.right);
                break;
        }

        expression.addContent(left);
        expression.addContent(operator);
        expression.addContent(right);
    }

    private void writeCustomLTL(Element ltl, CustomLTL customLTL)
    {
        // Create content element based on content string
        Element content = new Element("content");
        content.addContent(new Text(customLTL.getContent()));

        ltl.addContent(content);
    }
}
