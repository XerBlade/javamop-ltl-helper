package ltlAgentPkg.ioLML;

import ltlAgentPkg.LTLFile;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import java.io.IOException;

/**
 * Created by Rex Cope
 */
abstract class LMLReader {
    // Read LTL List from XML file at path
    public abstract LTLFile readXML(Element root) throws JDOMException, IOException;
}
