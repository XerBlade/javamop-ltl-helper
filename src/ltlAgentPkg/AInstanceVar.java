package ltlAgentPkg;

import static ltlAgentPkg.DataUtilities.isWhitespace;

/**
 * Created by Rex Cope
 *
 * Represents aspect instance variables
 */
public class AInstanceVar {
    private String type;        // data type
    private String id;          // variable name
    private String initializer; // code to initialize the variable (optional)
    private String description;
    private String requiredPackage; // Package the data type is from

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInitializer() {
        return initializer;
    }

    public void setInitializer(String initializer) {
        this.initializer = initializer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequiredPackage() {
        return requiredPackage;
    }

    public void setRequiredPackage(String requiredPackage) {
        this.requiredPackage = requiredPackage;
    }

    public AInstanceVar() {
        type = "";
        id = "";
        initializer = "";
        description = "";
        requiredPackage = "";
    }

    public AInstanceVar(String type, String name) {
        this.type = type;
        id = name;
        initializer = "";
        description = "";
        requiredPackage = "";
    }

    public String toLongString() {
        String representation = "";

        if (!isWhitespace(description))
            representation += "/* " + description + "*/\n\t";

        representation += type + " " + id;

        if (!isWhitespace(initializer))
            representation += " = " + initializer;

        representation += ";";

        return representation;
    }

}
