package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Abstract class for an expression with an operator
 *
 */


public abstract class OpExp extends Expression {
    private String operator;	// Operator the expression is using

    // Constructor defining type and operator
    OpExp(String type, String operator)
    {
        super(type);
        this.operator = operator;
    }

    public String getOperator()
    {
        return operator;
    }

    @Override
    public abstract String toLongString();
}
