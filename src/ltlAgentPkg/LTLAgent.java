package ltlAgentPkg;




import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

/**
 * Created by Rex Cope
 */

public class LTLAgent {
    private String name;	// Name given for this LTL
    public ArrayList<Event> events = new ArrayList<>();	// list of all events
    public LTL ltl;	// ltl definition
    private String description;		// Description of LTL
    public ArrayList<Parameter> parameters = new ArrayList<>();     // Static parameters
    public ArrayList<AInstanceVar> instanceVars = new ArrayList<>();    // instance variables
    private boolean matchFailCondition = false;  // Whether to use @violation (false) or @validation (true) for property handler
    private String propertyHandler;
    private ConcurrentSkipListSet<String> propertyPackages = new ConcurrentSkipListSet<>();
    private String mopPackage = "mop";
    private String modifier = "";   // whether there should be "connected" or "full-binding" or "decentralized" modifier

    // Default constructor
    public LTLAgent()
    {
        name = "LTL" + ((int) (Math.random()*(9999 - 1))) + 1;
        description = "";

        ltl = new HelperLTL();

        propertyHandler = "";

        propertyPackages.add("java.io.*");
        propertyPackages.add("java.util.*");
    }


    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setModifier(String modifier) {
        if (modifier.equals("full-binding") || modifier.equals("connected") || modifier.equals("decentralized"))
            this.modifier = modifier;
        else
            this.modifier = "";
    }

    public String getModifier() {
        return this.modifier;
    }

    public ConcurrentSkipListSet<String> getPropertyPackages() {
        return propertyPackages;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setMatchFailCondition(boolean condition) {
        matchFailCondition = condition;
    }

    public boolean getMatchFailCondition() {
        return matchFailCondition;
    }

    public void setPropertyHandler(String propertyHandler) {
        this.propertyHandler = propertyHandler;
    }

    public String getPropertyHandler() {
        return this.propertyHandler;
    }

    public void setMopPackage(String mopPackage) {
        this.mopPackage = mopPackage;
    }

    @SuppressWarnings("unused")
    public String getMopPackage() {
        return this.mopPackage;
    }

    // Set the type of LTL
    public void setType(String type)
    {
        if (type.equals("Helper"))
        {
            ltl = new HelperLTL();
        }
        else if (type.equals("Custom"))
        {
            ltl = new CustomLTL();
        }
    }

    // Get all the packages used anywhere in this property
    private ConcurrentSkipListSet<String> getAllPackages() {
        ConcurrentSkipListSet<String> allPackages = new ConcurrentSkipListSet<>(propertyPackages);

        allPackages.addAll(instanceVars.stream().filter(instanceVar -> !DataUtilities.isWhitespace(instanceVar.getRequiredPackage())).map(AInstanceVar::getRequiredPackage).collect(Collectors.toList()));

        for (Event event : events) {
            allPackages.addAll(event.getAllPackages());
        }

        allPackages.addAll(parameters.stream().filter(parameter -> !DataUtilities.isWhitespace(parameter.getPackage())).map(Parameter::getPackage).collect(Collectors.toList()));

        return allPackages;
    }


    public String toLongString(ArrayList<Event> globalEvents) {
        String representation = "";


        // Get mop package
        if (!DataUtilities.isWhitespace(mopPackage)) {
            representation += "package ";
            representation += mopPackage + ";\n\n";
        }

        // Import all packages
        ConcurrentSkipListSet<String> allPackages = getAllPackages();
        for (String currentPackage : allPackages) {
            representation += "import " + currentPackage + ";\n";
        }
        representation += "\n";


        representation += "/* \n" + description + " \n*/\n\n";

        if (!DataUtilities.isWhitespace(modifier))
            representation += modifier + " ";

        representation += name + "(";
        Iterator<Parameter> itemIterator = parameters.iterator();
        if (itemIterator.hasNext()) {
            // special-case first item.  in this case, no comma
            representation += itemIterator.next().toLongString();
            while (itemIterator.hasNext()) {
                representation += ", " + itemIterator.next().toLongString();
            }
        }
        representation += ") {\n";
        for (AInstanceVar instanceVar : instanceVars) {
            representation += "\t" + instanceVar.toLongString() + "\n";
        }
        representation += "\n";
        for (Event event : globalEvents) {
            representation += "\t" + event.toLongString() + "\n";
        }
        for (Event event : events) {
            representation += "\t" + event.toLongString() + "\n";
        }
        representation += "\n";
        representation += "\tltl : " + ltl.toLongString() + "\n\n";

        if (matchFailCondition) {
            representation += "\t@validation {\n";
        }
        else {
            representation += "\t@violation {\n";
        }
        representation += "\t\t" + propertyHandler + "\n\t}\n";

        representation += "}\n";

        return representation;
    }

    // Check if any events have any null expressions
    public Event eventHasNullExp() {
        for (Event event : events) {
            if (event == null || event.hasNullExp()) {
                return event;
            }
        }

        return null;
    }

}
