package ltlAgentPkg;

/**
 * @author Rex Cope
 *
 * Represent an LTL in plain text JavaMOP syntax
 *
 */


public class CustomLTL extends LTL {
    private String content;	// LTL in plain text JavaMOP input syntax

    // Default constructor
    CustomLTL()
    {
        super("Custom");
        content = "[] (event)";
    }

    // Constructor initializing content
    public CustomLTL(String content)
    {
        super("Custom");
        this.content = content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return this.content;
    }

    @Override
    public String toLongString() {
        return content;
    }

    @Override
    public boolean hasEvent(Event event) {
        return false;
    }

    @Override
    public Boolean hasNullExp() {
        return false;
    }

}
