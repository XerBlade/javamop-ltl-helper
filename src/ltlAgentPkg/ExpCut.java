package ltlAgentPkg;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by Rex Cope
 */
public class ExpCut extends Pointcut {
    private String prefix;
    private String content;

    ExpCut(String type) {
        super("exp");
        prefix = type;
        content = "";
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toLongString() {
        return prefix + "(" + content + ")";
    }

    @Override
    public boolean hasParameter(Parameter parameter) {
        return false;
    }

    @Override
    public Boolean hasNullExp() {
        return content.equals("");
    }

    @Override
    public Pointcut copy() {
        ExpCut copyOfThis = new ExpCut(this.prefix);
        copyOfThis.setContent(this.content);

        return copyOfThis;
    }

    @Override
    public ArrayList<Parameter> getAllUsedParameters() {
        return new ArrayList<>();
    }

    @Override
    public ConcurrentSkipListSet<String> getPackages() {
        return new ConcurrentSkipListSet<>();
    }
}
