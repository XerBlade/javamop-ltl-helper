package ltlAgentPkg;

/**
 * Created by Rex Cope
 */
public class Parameter {
    String type;       // data type
    private String name;        // ID
    private String description;
    private String requiredPackage; // Package the data type is from

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public String toLongString() {
        return type + " " + name;
    }

    public String getPackage() {
        return requiredPackage;
    }

    public void setPackage(String reqPackage) {
        requiredPackage = reqPackage;
    }

    public Parameter() {
        type = "";
        name = "";
        description = "";
        requiredPackage = "";
    }

    public Parameter(String type, String name) {
        this.type = type;
        this.name = name;
        this.description = "";
        requiredPackage = "";
    }
}
