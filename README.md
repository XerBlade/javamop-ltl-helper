Java program for helping users to generate input for JavaMOP.

The ltlAgentPkg package contains classes for defining all elements the JavaMOP LTL input.

The ltlFxGUI package contains classes for designing the user interface for helping to generate the objects from LTLAgentPkg.

To run the project through an IDE, load the whole repository into the IDE with the root directory of the repository as the main project folder.
Then add libs/JDOM 2.0.6/jdom-2.0.6.jar and libs/LTL2Bunchi/LTL2Buchi.jar as Libraries.
Run through main on main.java in the ltlFxGUI package.